Join the free fun
http://www.ModRiot.com
http://steamcommunity.com/groups/modriotgaming
http://www.svencoop.com
http://steamcommunity.com/groups/SvenCoop

As of August 21, 2016:

Sven Co-op Experience Mod AMXX plugin specially designed for Sven Co-op 5.0 - 5.05, and the ModRiotGaming server configurations for optimizing performance/stability.

The original SCXPM was written by "Silencer", and the base of this code is from edits made upon his plugin by "Wrd" + others in SCXPM version 17.31.3. I have received permission from Silencer to include his plugin concept with edits in this repository.

Any additions or changes to the code beyond version 17.31.3 A.K.A. "scxpm_wrd" have been made by Swamp Dog. Credits for participation in this project relating to debugging, diagnostics, and ideas will be found in "/documentation/scxpm_credits.txt". 

The current version in this repository is: 17.31.35. This is a work in progress. I am using this version as a milestone, as I believe this is the point where I will be adding more on to the code. I will try to offer support for the code, but it is really expected of anyone working with this code to have a knowledge of coding as well as how Sven Co-op server hosting works. If you have problems with this code, it is not my fault if you can't figure out how to use it fix it. I take no responsibilities for your troubles. The main part of the code which can be controlled with hard-coded defines are located in "/scripting/include/xpmod/define.inl". 

The main ideas behind this code were to stop breaking Sven Co-op maps (because, yes, SCXPM does break map function in certain cases), and to create a later loading time for all the code in order to slow down the amount of processing needed on map start as well as in game. Most of the help I received with diagnostics in this area can be atributed to JonnyBoy0719, and wouldn't have been possible without his help and ideas from his Sven Co-op RPG Mod. JonnyBoy0719's RPG Mod can be found @ https://github.com/jonnyboy0719/Sven-Coop-RPGMod . While RPG Mod can serve as a replacement or an alternative to SCXPM, I decided to take SCXPM to the next level of control since so many people like SCXPM. 

Full support has been added for Counter-Strike 1.6 (except for bots). Bot experience saving & loading is currently incomplete.

I added a skill-disabling feature with a permutations calculation I created with the CVARS for regeneration skills (Health Regeneration, Nano Armor, Ammo Reincarnation, Team Power, and Block Attack). By being able to disable any skill in any map, there is much more control over whether or not SCXPM will cause a map to malfunction. Any combination of CVARS designated to control skills being allowed in maps will load currently 1 of 31 loops specifically designed to allow only the skills which are enabled. All skills are enabled by default, and can be set in the AMXX per-map configuration located in "/svencoop/addons/amxmodx/configs/maps/mapname.cfg" (i. e. /svencoop/addons/amxmodx/configs/maps/desertcircle.cfg). I highly recommend disabling Health Regeneration, Nano Armor, and Team Power on any maps which have the entity "game_player_equip" such as desertcircle (see /svencoop/addons/amxmodx/configs/maps/desertcircle.cfg for example). Default cvar values are loaded in "/svencoop/addons/amxmodx/configs/amxx.cfg". 

The only thing I ask if you choose to use my permutations calculation code in any other plugin, please give me (swampdog@modriot.com) credit. I wrote the code from scratch and did not borrow any ideas from anyone. I enjoyed writing this code, and I hope you enjoy using it.

The cvars which control skills are: "scxpm_health", "scxpm_armor", "scxpm_hpregen", "scxpm_nano", "scxpm_ammo", "scxpm_gravity", "scxpm_teampower", "scxpm_block". Setting any of these cvars to "0" or less will turn them off, and setting them to "1" or greater will turn the skill on.

I created custom map configurations in "/svencoop/maps/mapname.cfg" or "/svencoop_addon/maps/mapname.cfg" to use "maxhealth" and "maxarmor" set to 645, because this is currently the maximum value of Sven Co-op Experience Mod skills. By doing this, it removes the need for the CPU to calculate health and regenerate it every second. The health and armor values do not "tick down" slowly but instead stay as they are.

To save/load your skills from a MySQL database, use the file "/svencoop/addons/amxmodx/configs/xp-sql.cfg". 

Custom load timing has been implemented due to Sven Co-op 5 calling "client_connect" more than once, and the unknown "client_authorized" function call has been removed. All new loading features can be found in "plugin_cfg" and "client_putinserver". Credit to JonnyBoy0719 for pointing this out to me so I could make a work-around solution with timers. This is one of the most important aspects of this newer version of SCXPM. The use of a better MySQL handling code may be better, but this is as good as it gets for now.

Any additional customizations may be explained in future documentation. A documentation directory will likely be created and will contain credits, changelog information, and any other notes about server customization possible.

More information will be included in a changelog in "/documentation/scxpm_changelog.txt". I may frequently change the code at times, and any updates which I think are vital will be included in the future in a newer version number.

Report bugs in this repository or by emailing swampdog@modriot.com
