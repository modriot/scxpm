This is not a complete changelog, but only a brief description of the skill changes made between SCXPM Version 17.31.4 & 17.31.21. The current version is 17.31.36, so this information below is outdated. Current changes have not been included since May 17, 2016. The latest update was August 22, 2016.
-
Sven Coop Experience Mod - Redesigned for Sven Co-op 5.0, with additions (ModRiot.com edition)
Based on version 17.31.4, last updated March 26, 2012
-
Code Changes Made by Swamp Dog
Credits to many people from ModRiot.com Sven Coop 5 servers. Credits will be listed from all the names of people who have participated in giving me any feedback, resources for code, ideas, and anything else I can think of.
-

August 22, 2016

List of Added Cvars as of today:

// enables or disables "Strength" skill. 1 to enable, 0 to disable
scxpm_health 1
// enables or disables "Superior Armor" skill. 1 to enable, 0 to disable
scxpm_armor 1
// enables or disables "Health Regeneration" skill. 1 to enable, 0 to disable
scxpm_hpregen 1
// enables or disables "Nano Armor" skill. 1 to enable, 0 to disable
scxpm_nano 1
// enables or disables "Ammo Reincarnation" skill. 1 to enable, 0 to disable
scxpm_ammo 1
// enables or disables "Anti-Gravity Device" skill. 1 to enable, 0 to disable
scxpm_gravity 1
// enables or disables "Team Power" skill. 1 to enable, 0 to disable
scxpm_teampower 1
// enables or disables "Block Attack" skill. 1 to enable, 0 to disable
scxpm_block 1
// sets the amount of frags/points needed to be awarded a medal or bonus xp
scxpm_fraglimit 100
// the amount of experience to multiply by the player's rank to determine the amount of bonus xp
scxpm_bonus 100
// the cost value for trading a medal for experience points, or experience points for medals (the "cost" multiplied by the rank of a player)
scxpm_tradevalue 100
// The amount of times the regeneration loop counts through a player's experience points before saving (stops low-level players from flooding the database when leveling up) - also counts how many times in between reaching the limit of scxpm_counter a player needs to reload their data if at a low level (I recommend not changing this, change at your own risk)
scxpm_counter 4
// Save control feature added to turn on or turn off the scxpm_counter factor in frequency of saving. Setting to 1 will turn save control on, 0 will turn it off. (I recommend not changing this, change at your own risk)
scxpm_savectrl 1
// Turn speed control on or off. To enable, set to 1, or to disable set to 0. Two uses - 1. The damage event will slow down the player when they are hurt, and their speed will be reduced with (maxspeed-scxpm_speedamt). The cvar below controls how much speed is subtracted from a player. 2. If an admin uses the speed control command on a player to "punish" them, the player will be slowed down by the same amount.
scxpm_speedctrl 0
// The amount of "speed" to subtract from a player's maxspeed if scxpm_speedctrl is set to "1" and the player is punished or being damaged
scxpm_speedamt 100
// The amount of times per map that a player may reach the "bonus" frags amount to win a medal or bonus xp. Default is 1 bonus per map.
scxpm_bonuslimit 1
// The amount of times per map that a player may trade a medal for experience points or trade experience points for a medal. Default is 1 trade per map.
scxpm_tradelimit 1
// Setting to give players free levels. Set to 1 to give players free levels, set to 0 to disable this feature.
scxpm_givefree 1
// The level to set players to if they are below this settinng's number and the free level setting is enabled. Default is 100 levels.
scxpm_freelevels 100
// The maximum amount of health total that a player can have in a map. Setting this to 0 disables this feature, and the maximum amount is 645. Setting this to anything other than 0 will set the max health of players to this number. Example: scxpm_maxhealth "100" will only allow for players to regeenrate health up to 100, even if their Strength skill is higher.
scxpm_maxhealth 0
// The maximum amount of armor total that a player can have in a map. Setting this to 0 disables this feature, and the maximum amount is 645. Setting this to anything other than 0 will set the max armor of all players to this number. Example: scxpm_maxhealth "100" will only allow for players to regeenrate armor up to 100, even if their Superior Armor skill is higher.
scxpm_maxarmor 0
// This setting can be enabled to allow for players to respawn with full health and armor from their skills. Set to 1 to enable, 0 to disable. 
scxpm_spawn 0

---


May 17, 2016

Changes to Skills:
Health:
Changed skill to loaded on spawn with ResetHUD event. Maximum HP is 645 (100+health+awareness+medals = 100+450+80+15)
Max Health of SCXPM is 645. Max Health of server on most maps is set to 645 in map config, so health does not "trickle down" anymore. 
On some maps, such as those like "desertcircle" with game_player_equip entities, the Health skill may need to be disabled.
Armor:
Changed skill to loaded on spawn with ResetHUD event. Maximum AP is 645 (100+armor+awareness+medals = 100+450+80+15)
Max Armor of SCXPM is 645. Max Armor of server on most maps is set to 645 in map config, so armor does not "trickle down" anymore. 
On some maps, such as those like "desertcircle" with game_player_equip entities, the Armor skill may need to be disabled.
Health Regeneration:
Skill will regenerate up to 100+Health+Awareness+Medals (max 645 HP).
Regeneration rate variable based on Health Regeneration level (max 300), Awareness (max 80), and Medals (max 15). Every second, skill points + medals will add up to 380 (Health Regeneration level 300+Awareness level 80). This is one of the skills that medals can be pretty helpful with. Once the counter reaches >= 380, the player will get +1 HP, and the counter will reset to (Health Regeneration Level + Awareness Level + Medals Amount) (max 395). 
If the player has less than 100 HP and holds out Medkit, it will quickly add points back to health. If player has 100 or more HP and holds out Medkit, it will increase the Health Regeneration rate twice as fast as when the player is not holding out the Med Kit.
Health Regeneration skill has been set to stop for 5 seconds on receiving damage (from an enemy or other cases). The code has been changed to use the EVENT_Damage idea from Jonnyboy0719's RPG Mod. When getting attacked by an enemy, the player's Health Regeneration skill will be ineffective for a variable amount of time (currently 5 seconds).
There is a possibility this part of the code may have bugs with Sven Coop 5 "Damage" event and AMXX Engine module. I have set so there is only a registration for the "Damage" event when a player is alive. This may or may not be helpful to reducing code processing, but also adds more checks.
This feature may experience bugs with the "Block Attack" skill if it is causing interference with the "Damage" event. It was necessary to include the Block Attack event with the disabled regeneration ability due to interference. Using Block Attack to try to bypass the Damage event code was a bad idea because it completely disabled the Damage event altogether.
On some maps, such as those like "desertcircle" with game_player_equip entities, the Health Regeneration skill may need to be disabled.
Nano Armor:
Skill will regenerate up to 100+Armor+Awareness+Medals (max 645 AP).
Regeneration rate variable based on Nano Armor level (max 300), Awareness (max 80), and Medals (max 15). Every second, skill points + medals will add up to 380 (Nano Armor level 300+Awareness level 80). This is one of the skills that medals can be pretty helpful with. Once the counter reaches >= 380, the player will get +1 AP, and the counter will reset to (Nano Armor Level + Awareness Level + Medals Amount) (max 395). 
If the player has less than 100 Armor and holds out Crowbar or Pipewrench, it will quickly add points back to armor. If player has 100 or more AP and holds out Crowbar or Pipewrench, it will increase the Nano Armor rate twice as fast as when the player is not holding out the Crowbar or Pipewrench.
Nano Armor skill has been set to stop for 5 seconds on receiving damage (from an enemy or other cases). The code has been changed to use the EVENT_Damage idea from Jonnyboy0719's RPG Mod. When getting attacked by an enemy, the player's Nano Armor skill will be ineffective for a variable amount of time (currently 5 seconds).
There is a possibility this part of the code may have bugs with Sven Coop 5 "Damage" event and AMXX Engine module. I have set so there is only a registration for the "Damage" event when a player is alive. This may or may not be helpful to reducing code processing, but also adds more checks.
This feature may experience bugs with the "Block Attack" skill if it is causing interference with the "Damage" event. It was necessary to include the Block Attack event with the disabled regeneration ability due to interference. Using Block Attack to try to bypass the Damage event code was a bad idea because it completely disabled the Damage event altogether.
On some maps, such as those like "desertcircle" with game_player_equip entities, the Nano Armor skill may need to be disabled.


From "scxpm_wrd" version 17.31.4 changelog:
/*
*	Future Features
*	if you want new features ask it on: http://forums.alliedmods.net/showthread.php?t=44168
*	------------------
*	- pruning database with the `lastUpdated` field
*	- cuicide penalty
*
*/

/*
*	Changelog
*
*	Version:	17.31.4
*	Date:		26 March 2012
*	------------------------
*	Added an optional ability to limit maximum level that players can gain per map. This will prevent players to use bugs to boost their XP (e.g of4a4.bsp)
*	Added cvars 'scxpm_maxlevelup_enabled', 'scxpm_maxlevelup' and 'scxpm_maxlevelup_limit'
*	------------------------
*
*	Version:	17.31.3
*	Date:		4 October 2011
*	------------------------
*	Bug Teampower give no hp and ap to Teammates
*	Fixed Remove wrong code ({// Do nothing} else ) to fix teampower
*	Edited Teampower Speed from 2.0 to 1.0
*	Edited Teampower +60 hp and ap too +100 hp and ap
*	Edited Teampower luck from 4200 to 2000
*	Edited Teampower luck by 40 skill points from 0,1000 to 1000,1000
*	Edited Block Attack luck from 0,185 to 80,185 to fix it but dont know if it works
*	Edited Medikit Heal speed from slow to fast
*	Edited scxpm_hud_channel" from 3 to 0
*	------------------------
*
*	Version:	17.31.2
*	Date:		2 October 2011
*	------------------------
*	Fixed Changed new starthealth; and new startarmor; in new starthealth=100; and new startarmor=100;to fix the bug by join the game hp still by 100 hp.
*	Fixed scxpm_regen and I fixed the backward hp problem when you have over 100 hp
*	Edited armor regeneration
*	Edited scxpm_randomammo
*	Edited give stuff (ammo regeneration ,ammo packs)
*	Edited scxpm_xpgain from 1.0 to 10.0
*	Edited scxpm_save_frequent from 0 to 1
*	------------------------
*
*	Version:	17.31.1
*	Date:		11 February 2011
*	------------------------
*	- Changed boolean structures(loaddata, has_godmode, etc) - now it's
*	  not 33 sized bool arrays, but single ints
*	- Hooking say event instead of concmd every say command
*	- Restructured rank data, ranks are now in a variable, only a rank's id is stored
*	- Changed cvars into pcvars
*	- Removed some stuff:
*	  - #include <core> - this is already included in amxmodx
*	  - load_error array - never gets a true value, so it is unused
*	------------------------
*
*	Version: 	17.31
*	Date: 		21 March 2010
*	------------------------
*	- Fixed movement bug
*	- Fixed not receiving xp for kill
*	------------------------
*
*	Version: 	17.30
*	Date: 		08 Februari 2010
*	------------------------
*	- Added Incremental skill upgrade
*	------------------------
*
*	Version: 	17.29
*	Date: 		07 Februari 2010
*	------------------------
*	- Fixed: set loaddata to true when loading from file
*	------------------------
*
*	Version: 	17.28
*	Date: 		02 Februari 2010
*	------------------------
*	- Fixed: don't set the xp back to 0
*	------------------------
*
*	Version: 	17.27
*	Date: 		20 December 2009
*	------------------------
*	- Added: scxpm_maxlevel cvar for capping the max level
*	- Fixed: xp problem when #define USING_CS is commented
*	------------------------
*
*	Version: 	17.26
*	Date: 		15 November 2009
*	------------------------
*	- Fixed: The inefficient way of calculating the xp does not work for sven coop 
*	------------------------
*
*	Version: 	17.25
*	Date: 		9 November 2009
*	------------------------
*	- Changed: amx_scxpm_gamename to scxpm_gamename
*	- Changed: amx_scxpm_save to scxpm_save
*	- Changed: amx_scxpm_savestyle to scxpm_savestyle
*	- Changed: amx_scxpm_debug to scxpm_debug
*	- Changed: scxpm_xpgain to scxpm_xpgain
*	------------------------
*
*	Version: 	17.24
*	Date: 		8 November 2009
*	------------------------
*	- Removed: Inefficient way of calculating the xp
*	- Added: Event triggered way of calculating the xp
*	------------------------
*
*	Version: 	17.23
*	Date: 		7 November 2009
*	------------------------
*	- Added: lastupdate field in the database for later use
*	- Added: cvar scxpm_minplaytime: setting in seconds the minimum playing time before stats are saved
*	- Fixed: empty uniqueid added to database
*	- Fixed: empty ip or nickname added to the database
*	------------------------
*
*	Version: 	17.22
*	Date: 		2 November 2009
*	------------------------
*	- Changed database structure and queries for better query handling
*	- Added an check so that data is not loaded twice because of retreiving the steamid
*	------------------------
*
*	Update Note from version 17.21 to 17.22
*	execute the following query:	
*	- ALTER TABLE `scxpm_stats` ADD `uniqueid` VARCHAR( 50 ) NOT NULL AFTER `id` ;
*	- UPDATE scxpm_stats SET uniqueid = <unique>;
*		replace <unique> with the following possibilities according to your situation
*			- authid
*			- ip
*			- nick
*	-  ALTER TABLE `scxpm_stats` ADD UNIQUE (`uniqueid`) 
*
*	Version: 	17.21
*	Date: 		28 Oktober 2009
*	------------------------
*	- Fixed: xp lower than 0 won't be saved
*	------------------------
*
*	Version: 	17.20
*	Date: 		11 Oktober 2009
*	------------------------
*	- Fixed: Possible entries inserted in the database with no auth id when the savestyle is on authid
*   - Fixed: Players could only gain one level and the rest would not be saved
*	- Fixed: directly go to the correct level when alot of xp is given, instead of going through every level
*   - Added: More debug feedback
*	------------------------
*
*	Version: 	17.19
*	Date: 		 8 Oktober 2009
*	------------------------
*	- Fixed: Added the loaddata check on save, no stats will be saved if no data is loaded
*	------------------------
*
*	Version: 	17.18
*	Date: 		15 September 2009
*	------------------------
*	- Fixed: Threaed SQL (There were incredible horrible bugs)
*	------------------------
*
*	Version: 	17.17
*	Date: 		13 September 2009
*	------------------------
*	- Added: Threaded sql
*	------------------------
*
*	Version: 	17.16
*	Date: 		31 August 2009
*	------------------------
*	- Fixed: Showing the players info and other players info 
*	------------------------
*
*	Version: 	17.15
*	Date: 		28 August 2009
*	------------------------
*	- Added create sql script info
*	- Fixed the savexp_all sql part
*	- Fixed health issue
*	------------------------
*
*
*	Version: 	17.14
*	Date: 		2 August 2009
*	------------------------
*	- Added changes made by supergreg
*	 = Changed the database cvars
*      Now another database can be used instead of the default one
*	 = Fixed: cs weapons offset
*	 = Generally small but useful changes
*	 = Added a define for cs, if you don't use this pluging for cs please comment the define
*   ------------------------
*
*	Version: 	17.13
*	Date: 		30 Juli 2009
*	------------------------
*	- Fixed repeated saving stats; scxpm_save_frequent fixed
*   ------------------------
*
*	Version: 	17.12
*	Date: 		30 Juli 2009
*	-------------------------
*	- Try to fix the health bug when spawning
*	- Added this changelog	
*	-------------------------
*/