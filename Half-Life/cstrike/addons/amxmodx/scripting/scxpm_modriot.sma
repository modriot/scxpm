/* SCXPM Version 17.0 by Silencer
** Edited by Wrd, Version 17.31
** 	Wrd thanks supergreg for his suggestions
**
** Edited by PythEch, Version 17.31.4
** 
** Special Thanks to:
** 
** VEN			For heavily improving my Scripting-Skills.  ;p 
** darkghost9999	For his great Ideas!
** 
** 
** Thanks to:
** 
** ThomasNguyen
** `666
** g3x
** Hitman (Warchild)
** 
*/

/* SCXPM Version 17.31.21 edited by Swamp Dog @ ModRiot.com */
/* Further Credits from Sven Coop 5.0 development by players from ModRiot.com will be included */
#include <amxmodx>
#include <amxmisc>
#include <fakemeta>
#include <fun>
#include <sqlx>
#include <bit>

#include "xpmod/define.inl"
#include "xpmod/client.inl"
#include "xpmod/event.inl"
#include "xpmod/menu.inl"
#include "xpmod/hud.inl"
#include "xpmod/skill.inl"
#include "xpmod/cmd.inl"
#include "xpmod/main.inl"
#include "xpmod/xp.inl"
#include "xpmod/db.inl"

#if defined ALLOW_MONSTERS
#include "xpmod/mxp.inl"
#endif
public plugin_init()
{
//#if !defined USING_SVEN
//	log_amx( "[SCXPM] Loading Sven Coop XP Mod version %s", VERSION );
//#endif
	register_plugin("SCXPM",VERSION,"Silencer, Swamp Dog");
	//register_dictionary("scxpm.txt");
	register_menucmd(register_menuid("Select Skill"),(1<<0)|(1<<1)|(1<<2)|(1<<3)|(1<<4)|(1<<5)|(1<<6)|(1<<7)|(1<<8)|(1<<9),"SCXPMSkillChoice");
	register_menucmd(register_menuid("Select Increment"),(1<<0)|(1<<1)|(1<<2)|(1<<3)|(1<<4)|(1<<5),"SCXPMIncrementChoice");
#if !defined USING_SVEN
	register_forward(FM_GetGameDescription,"scxpm_gn"); // I usually load this forward in _server_start, so I made a separate plugin "gamename.sma"
#endif

	// allow for bots in ResetHUD?
#if !defined ALLOW_BOT_EVENT // needed a new define, because ALLOW_BOTS is not enough control here
#if defined EVENT_DMG
	register_event("Damage", "EVENT_Damage", "bef")
#endif
	register_event("ResetHUD", "on_ResetHUD", "bef"); // bef - b = only single client, e = alive, and f = only humans
#else
#if defined EVENT_DMG
	register_event("Damage", "EVENT_Damage", "be")
#endif
	register_event("ResetHUD", "on_ResetHUD", "be"); // bef - b = only single client, e = alive
#endif
	
	register_concmd("amx_setlvl","scxpm_setlvl", ADMIN_LEVEL_B,"Playername Value - Will set Players Level");
	register_concmd("amx_addmedal","scxpm_addmedal", ADMIN_LEVEL_B,"Playername - Will award Player with a Medal");
	register_concmd("amx_removemedal","scxpm_removemedal", ADMIN_LEVEL_B,"Playername - Will remove a Medal of a Player");
	register_concmd("amx_addxp","scxpm_addxp", ADMIN_LEVEL_B,"Playername Value - Will add xp to Players xp");
	register_concmd("amx_removexp","scxpm_removexp", ADMIN_LEVEL_B,"Playername Value - Will remove xp from Players xp");
	// for debug reasons
	// register_concmd("say savedata","scxpm_savexp_all_mysql",ADMIN_IMMUNITY,"- Will save your SCXPM data");
	register_concmd("amx_godmode","scxpm_godmode", ADMIN_LEVEL_B,"Playername - Toggle Players God Mode On or Off.");
	register_concmd("amx_noclipmode","scxpm_noclipmode", ADMIN_LEVEL_B,"Playername - Toggle Players noclip Mode On or Off.");
#if defined SPEED_CTRL
	register_concmd("amx_speedctrl", "scxpm_speed", ADMIN_LEVEL_A, "<#userid,nick,SteamID> - Turn speed control on or off for player")
#endif
	register_clcmd("say", "say_hook");
	register_clcmd("say_team", "say_hook");
	register_concmd("selectskills","SCXPMSkill",0,"- Opens the Skill Choice Menu, if you have Skillpoints available");
	register_concmd("selectskill","SCXPMSkill",0,"- Opens the Skill Choice Menu, if you have Skillpoints available");
	register_concmd("resetskills","scxpm_reset",0,"- Will reset your Skills so you can rechoose them");
	// register_concmd("playerskills","scxpm_others",0,"- Will print Other Players Stats");
	// register_concmd("skillsinfo","scxpm_info",0,"- Will print Information about all Skills");
	register_concmd("skillsinfo","scxpm_skillsinfo",0,"- Will print Information about all Skills");
	register_concmd("scxpminfo","scxpm_version",0,"- Will print Information about SCXPM");
#if !defined USING_SVEN
	pcvar_gamename = register_cvar("scxpm_gamename","1");
#endif
	// may decide to use this in CS or other mods, but I doubt it at this point.
#if !defined USING_CS
#if !defined USING_GW
	/* If enabled, it will try to prevent players using bugs to boost their XP in maps like of4a4
	** 0 = Disabled
	** 1 = Enabled
	*/
	pcvar_maxlevelup_enabled = register_cvar("scxpm_maxlevelup_enabled", "1");
	// Maximum level can be gained per map (Default:20)
	pcvar_maxlevelup = register_cvar("scxpm_maxlevelup", "20");
	// Players will be able to level up without limitations if they're under the specified level (Default:100)
	pcvar_maxlevelup_limit = register_cvar("scxpm_maxlevelup_limit", "100");
#endif
#endif
	// to stop scxpm from breaking maps - disable gravity on certain maps in "mapname.cfg" in configs/maps dir - swmpdg
	// set to 0 to disable gravity
	pcvar_gravity = register_cvar("scxpm_gravity", "1");
#if defined FRAGBONUS
	pcvar_fraglimit = register_cvar("scxpm_fraglimit", "100")
	// bonus is scxpm_bonus*rank[id], so level 20 would be 20*100. May still be too high, but it is a medal...
	// sell medal for same prices as scxpm_bonus*rank[id]
	pcvar_bonus = register_cvar("scxpm_bonus", "100")
#endif
	
#if defined SPEED_CTRL
	pcvar_speedctrl = register_cvar("scxpm_speedctrl","1")
	pcvar_reduce = register_cvar("scxpm_speedamt", "100")
#endif

#if defined COUNT_CTRL

	// controls rate of how fast the counter goes for reexp and showdata (from 0-5, 4=5 seconds at regen rate of 1.0 second loop)
	pcvar_counter = register_cvar("scxpm_counter", "4")
#if !defined USING_CS
#if !defined USING_GW
	// controls rate (counter) of saving data if player does not gain a level - value of "60" with regen loop running once every second = once a minute.
	pcvar_savectrl = register_cvar("scxpm_savectrl", "1")
#endif
#endif

#endif
	
	/* 
	** set the save style
	** 0 = not saved
	** 1 = save in a file
	** 2 = save in mysql  
	*/
	pcvar_save = register_cvar("scxpm_save","2");
	// for debug information in the logfile set this to 1
	pcvar_debug = register_cvar("scxpm_debug","0");
	/*
	** what to save on
	** 0 = id
	** 1 = ip address
	** 2 = nickname
	*/
	//register_cvar("scxpm_savestyle", "0")
	// if sv_lan is on save stuff on the nickname
	if (get_cvar_num("sv_lan") == 1)
	{
		pcvar_savestyle = register_cvar("scxpm_savestyle", "2");
	}
	else
	{
		pcvar_savestyle = register_cvar("scxpm_savestyle", "0");
	}
	// minimum play time before saving data in seconds, 0 = always save
	pcvar_minplaytime = register_cvar("scxpm_minplaytime", "0");
	/*
	** mysql
	*/
	// need to recode for security purposes - swmpdg
#if defined SQLCVAR
	pcvar_sql_host	= register_cvar("scxpm_sql_host", "127.0.0.1");
	pcvar_sql_user	= register_cvar("scxpm_sql_user", "NOTSET");
	pcvar_sql_pass	= register_cvar("scxpm_sql_pass", "NOTSET");
	pcvar_sql_db	= register_cvar("scxpm_sql_db", "NOTSET");
#endif
	pcvar_sql_table	= register_cvar("scxpm_sql_table", "scxpm_stats");

#if !defined USING_SVEN
#if !defined USING_CS
#if !defined SQLCVAR
	new configsDir[64];
	get_configsdir(configsDir, 63);
	server_cmd("exec %s/xp-sql.cfg", configsDir);
#endif
#endif
#endif

#if defined USING_CS
	ammotype[0] = "ammo_9mm";	// glock18, elite, mp5navy, tmp
	ammotype[1] = "ammo_50ae";	// deagle
	ammotype[2] = "ammo_buckshot";	// m3, xm1014
	ammotype[3] = "ammo_57mm";	// p90, fiveseven
	ammotype[4] = "ammo_45acp";	// usp, mac10, ump45
	ammotype[5] = "ammo_556nato";	// famas, sg552, m4a1, aug, sg550
	ammotype[6] = "ammo_357sig";	// p228? (was 9mm, forgot to change.)
#endif
#if defined USING_GW
	ammotype[0] = "ammo_9mm";	// glock18, ump45, mp5navy, tmp, mac 10
	ammotype[1] = "ammo_50ae";	// magnum
	ammotype[2] = "ammo_buckshot";	// m3, xm1014
	ammotype[3] = "ammo_357sig";	// beretta
	ammotype[4] = "ammo_762nato";	// ak47, ak74, sg550
	ammotype[5] = "ammo_556nato";	// sg552, m16
//	ammotype[6] = "ammo_45acp";	// usp
#endif
	// hud message fix if it conflicts with other plugins
	pcvar_hud_channel = register_cvar("scxpm_hud_channel", "0");
	// to enable frequent savestyle
	// if set to 1 players data will be saved as soon it gains xp
	pcvar_save_frequent = register_cvar("scxpm_save_frequent", "0");
	pcvar_xpgain = register_cvar( "scxpm_xpgain", "5.0" ); // default 10, changed to 5
	// possibility to cap the max level
	pcvar_maxlevel = register_cvar( "scxpm_maxlevel", "1800" );
	
	// moved to plugin_cfg for svencoop - need to do same for cs, to load in client_putinserver...
/*
#if !defined USING_SVEN
	// no roundstart in svencoop
	register_logevent("roundstart", 2, "0=World triggered", "1=Round_Start");
#endif
*/

#if defined USING_CS
	register_event("DeathMsg","death","a");
#endif
	// added monster xp support
#if defined ALLOW_MONSTERS
	// DO NOT EDIT THIS FILE TO CHANGE CVARS, USE THE SHCONFIG.CFG
	AgrXP = register_cvar("mxp_agrunt_xp", "15")
	ApaXP = register_cvar("mxp_apache_xp", "20")
	BarXP = register_cvar("mxp_barney_xp", "15")
	BigXP = register_cvar("mxp_bigmomma_xp", "25")
	BulXP = register_cvar("mxp_bullsquid_xp", "5")
	ConXP = register_cvar("mxp_controller_xp", "15")
	GarXP = register_cvar("mxp_gargantua_xp", "50")
	HeaXP = register_cvar("mxp_headcrab_xp", "3")
	HouXP = register_cvar("mxp_houndeye_xp", "5")
	HasXP = register_cvar("mxp_hassassin_xp", "15")
	HgrXP = register_cvar("mxp_hgrunt_xp", "15")
	SciXP = register_cvar("mxp_scientist_xp", "1")
	IslXP = register_cvar("mxp_islave_xp", "10")
	SnaXP = register_cvar("mxp_snark_xp", "1")
	ZomXP = register_cvar("mxp_zombie_xp", "10")

	//HAMSANDWICH
	RegisterHam(Ham_Killed, "func_wall", "monster_killed", 1)
#endif
}
/* AMXX-Studio Notes - DO NOT MODIFY BELOW HERE
*{\\ rtf1\\ ansi\\ deff0{\\ fonttbl{\\ f0\\ fnil Tahoma;}}\n\\ viewkind4\\ uc1\\ pard\\ lang1031\\ f0\\ fs16 \n\\ par }
*/