// vim: set ts=4 sw=4 tw=99 noet:
//
// AMX Mod X, based on AMX Mod by Aleksander Naszko ("OLO").
// Copyright (C) The AMX Mod X Development Team.
//
// This software is licensed under the GNU General Public License, version 3 or higher.
// Additional exceptions apply. For full license details, see LICENSE.txt or visit:
//     https://alliedmods.net/amxmodx-license

//
// Counter-Strike Functions
//

#if defined _gangwars_const_included
	#endinput
#endif
#define _gangwars_const_included

/**
 * IDs of weapons in GangWars
 */
#define WEAPON_92D            1
#define WEAPON_GLOCK18        2
#define WEAPON_DEAGLE         3
#define WEAPON_TMP            4
#define WEAPON_MAC10          5
#define WEAPON_UMP45          6
#define WEAPON_MP5NAVY        7
#define WEAPON_AK47           8
#define WEAPON_M16            9
#define WEAPON_SG552          10
#define WEAPON_AUG            11
#define WEAPON_SG550          12
#define WEAPON_M3             13
#define WEAPON_XM1014         14
#define WEAPON_HEGRENADE      30
#define WEAPON_KNIFE          31
