// "xp.inl" for xp handling organization

// may need to make negative values for all these functions to calculate xp on negative amount of frags

// calculate needed xp for next level
// I may want to use this example to make medal gain variable by float value and player level instead of rank...
// calculate needed xp for next level
public scxpm_calcneedxp ( id ) {
	new Float:m70 = float( playerlevel[id] ) * 70.0;
	new Float:mselfm3dot2 = float( playerlevel[id] ) * float( playerlevel[id] ) * 3.5;
	neededxp[id] = floatround( m70 + mselfm3dot2 + 30.0 );
}
// calculate level from xp
public scxpm_calc_lvl ( xp ) {
	return floatround( -10 + floatsqroot( 100 - ( 60 / 7 - ( ( xp - 1 ) / 3.5 ) ) ), floatround_ceil );
}
public scxpm_calc_xp ( level) {
	level--;
	return floatround( (float( level ) * 70.0) + (float( level ) * float(level) * 3.5) + 30);
}
// Task loop function
// regen task loop start
public scxpm_sdac(id) 
{
#if defined COUNT_CTRL
	// here is the loop task... added in counter variables here for being used in skill.inl scxpm_regen. 
	if(is_user_connected(id))
	{
		if (playerlevel[id]<get_pcvar_num(pcvar_maxlevel) && count_save[id]!=0)
		{
			count_save[id] = 0
		}
		// can take out bot check in HUD code and scxpm_regen if doing check before loading?
		if(!is_user_bot(id))
		{
			//scxpm_showdata(id);	// use counter in scxpm_regen
			client_print(id, print_chat, "[SCXPM] Please wait for your skills to be loaded.");
			set_task(1.0, "scxpm_regen", id, "", 0, "b")
		}
#if defined ALLOW_BOTS
		else
		{
			set_task(1.0, "scxpm_regen", id, "", 0, "b")
		}
#endif	
	}
#endif
}
/* AMXX-Studio Notes - DO NOT MODIFY BELOW HERE
*{\\ rtf1\\ ansi\\ deff0{\\ fonttbl{\\ f0\\ fnil Tahoma;}}\n\\ viewkind4\\ uc1\\ pard\\ lang1031\\ f0\\ fs16 \n\\ par }
*/