// "menu.inl" 

// show skill calculation
public SCXPMSkill( id ) {
	// the default value for the increment is 1
	skillIncrement[id] = 1;
	if (skillpoints[id] > 20 ) {
#if !defined ALLOW_BOTS
		if(!is_user_bot(id))
		{
			SCXPMIncrementMenu( id );
		}
#else
		SCXPMIncrementMenu( id );
#endif
	}
	else {
#if !defined ALLOW_BOTS
		if(!is_user_bot(id))
		{
			SCXPMSkillMenu( id );
		}
#else
		SCXPMSkillMenu( id );
#endif
	}
}
// show the skills menu
public SCXPMSkillMenu( id ) {
	new menuBody[1024];
	format(menuBody,1023,"Select Skills - Skillpoints available: %i^n^n^n^n 1.   Strength  [ %i / 450 ]^n^n 2.   Superior Armor  [ %i / 450 ]^n^n 3.   Health Regeneration  [ %i / 300 ]^n^n 4.   Nano Armor  [ %i / 300 ]^n^n 5.   Ammo Reincarnation  [ %i / 30 ]^n^n 6.   Anti Gravity Device  [ %i / 40 ]^n^n 7.   Awareness  [ %i / 80 ]^n^n 8.   Team Power  [ %i / 60 ]^n^n 9.   Block Attack  [ %i / 90 ]^n^n^n 0.   Done"
	,skillpoints[id],health[id],armor[id],rhealth[id],rarmor[id],rammo[id],gravity[id],speed[id],dist[id],dodge[id]);
	show_menu(id,(1<<0)|(1<<1)|(1<<2)|(1<<3)|(1<<4)|(1<<5)|(1<<6)|(1<<7)|(1<<8)|(1<<9),menuBody,13,"Select Skill");
}
// show the increment menu
public SCXPMIncrementMenu( id ) {
	new menuBody[1024];
	if (skillpoints[id] >= 20 && skillpoints[id] < 50) {
		format(menuBody,1023,"Increment your skill with^n^n^n^n1.    1  point^n^n2.    5  points^n^n3.    10 points^n^n4.    25 points");
	}
	else if (skillpoints[id] >= 50 && skillpoints[id] < 100) {
		format(menuBody,1023,"Increment your skill with^n^n^n^n1.    1  point^n^n2.    5  points^n^n3.    10 points^n^n4.    25 points^n^n5.    50 points");
	}
	else if (skillpoints[id] >= 100) {
		format(menuBody,1023,"Increment your skill with^n^n^n^n1.    1  point^n^n2.    5  points^n^n3.    10 points^n^n4.    25 points^n^n5.    50 points^n^n6.    100 points");
	}
	show_menu(id,(1<<0)|(1<<1)|(1<<2)|(1<<3)|(1<<4)|(1<<5),menuBody,13,"Select Increment");
}
public SCXPMIncrementChoice( id, key ) {
	switch(key){
		case 0: {
			skillIncrement[id] = 1;
		}
		case 1: {
			skillIncrement[id] = 5;
		}
		case 2: {
			skillIncrement[id] = 10;
		}
		case 3: {
			skillIncrement[id] = 25;
		}
		case 4: {
			skillIncrement[id] = 50;
		}
		case 5: {
			skillIncrement[id] = 100;
		}
	}
	SCXPMSkillMenu( id );
}
// choose a skill
public SCXPMSkillChoice( id, key ) {
	switch(key)
	{
		case 0:
		{
			if(skillpoints[id]>0)
			{
				if(health[id]<450)
				{
					if (skillIncrement[id] + health[id] >= 450) 
					{
						skillIncrement[id] = 450 - health[id];
					}
					// this is where the skill point bug is? why is the part above needed???....
					// adding in over-flow check for skillIncrement (bug with adding negative skillpoints... hopefully fixed - swmpdg)
					// now there is an overflow in health points given... 
					if (skillpoints[id] >= skillIncrement[id])
					{
						skillpoints[id] -= skillIncrement[id];
						health[id] += skillIncrement[id];
						client_print(id,print_chat,"[SCXPM] You spent %i Skillpoints to enhance your Strength to Level %i!",skillIncrement[id],health[id]);
						if(is_user_alive(id))
						{
							set_user_health(id, get_user_health(id) + skillIncrement[id]);
							// temporary fix...
							if(get_user_health(id) > 645)
							{
								set_user_health(id, 645)
							}
						}
					}
					else
					{
						skillIncrement[id] = skillpoints[id];
						skillpoints[id] -= skillIncrement[id];
						health[id] += skillIncrement[id];
						client_print(id,print_chat,"[SCXPM] You spent %i Skillpoints to enhance your Strength to Level %i!",skillIncrement[id],health[id]);
						if(is_user_alive(id))
						{
							set_user_health(id, get_user_health(id) + skillIncrement[id]);
						}
					}
				}
				else
				{
					client_print(id,print_chat,"[SCXPM] You have mastered your Strength already.")
				}
				if(skillpoints[id]>0)
				{
					SCXPMSkill(id);
				}
			}
			else
			{
				client_print(id,print_chat,"[SCXPM] You need one Skillpoint for enhancing your Strength.")
			}
		}
		case 1:
		{
			if(skillpoints[id]>0)
			{
#if defined USING_GW
				client_print(id,print_chat,"[SCXPM] Superior Armor skill is currently disabled in GangWars");
#else
				if(armor[id]<450)
				{
					if (skillIncrement[id] + armor[id] >= 450) 
					{
						skillIncrement[id] = 450 - armor[id];
					}
					if (skillpoints[id] >= skillIncrement[id])
					{
						skillpoints[id]-= skillIncrement[id];
						armor[id] += skillIncrement[id];
						client_print(id,print_chat,"[SCXPM] You spent %i Skillpoints to enhance your Armor to Level %i!",skillIncrement[id],armor[id]);
						if(is_user_alive(id))
						{
#if defined USING_CS
							cs_set_user_armor(id,get_user_armor(id)+skillIncrement[id], CS_ARMOR_VESTHELM);
#else
							set_user_armor(id,get_user_armor(id)+skillIncrement[id]);
#endif
							// temporary fix for overflow
							if(get_user_armor(id)>645)
							{
#if defined USING_CS
								cs_set_user_armor(id, 645, CS_ARMOR_VESTHELM);
#else
								set_user_armor(id, 645);
#endif	
							}
						}
					}
					else
					{
						skillIncrement[id] = skillpoints[id];
						skillpoints[id]-= skillIncrement[id];
						armor[id] += skillIncrement[id];
						client_print(id,print_chat,"[SCXPM] You spent %i Skillpoints to enhance your Armor to Level %i!",skillIncrement[id],armor[id]);
						if(is_user_alive(id))
						{
#if defined USING_CS
							cs_set_user_armor(id,get_user_armor(id)+skillIncrement[id], CS_ARMOR_VESTHELM);
#else
							set_user_armor(id,get_user_armor(id)+skillIncrement[id]);
#endif
						}
					}
				}
				else
				{
					client_print(id,print_chat,"[SCXPM] You have mastered your Armor already.")
				}
				if(skillpoints[id]>0)
				{
					SCXPMSkill(id)
				}
#endif
			}
			else
			{
				client_print(id,print_chat,"[SCXPM] You need one Skillpoint for enhancing your Armor.")
			}
		}
		case 2:
		{
			if(skillpoints[id]>0)
			{
				if(rhealth[id]<300)
				{
					if (skillIncrement[id] + rhealth[id] >= 300) {
						skillIncrement[id] = 300 - rhealth[id];
					}
					if (skillpoints[id] >= skillIncrement[id])
					{
						skillpoints[id] -= skillIncrement[id];
						rhealth[id] += skillIncrement[id];
						// insert healthwait code here? - swmpdg
						rhealthwait[id]=rhealth[id]
						client_print(id,print_chat,"[SCXPM] You spent %i Skillpoints to enhance your Regeneration to Level %i!",skillIncrement[id],rhealth[id])
					}
					else
					{
						skillIncrement[id] = skillpoints[id];
						skillpoints[id] -= skillIncrement[id];
						rhealth[id] += skillIncrement[id];
						// insert healthwait code here? - swmpdg
						rhealthwait[id]=rhealth[id]
						client_print(id,print_chat,"[SCXPM] You spent %i Skillpoints to enhance your Regeneration to Level %i!",skillIncrement[id],rhealth[id])
					}
				}
				else
				{
					client_print(id,print_chat,"[SCXPM] You have mastered your Regeneration already.")
				}
				if(skillpoints[id]>0)
				{
					SCXPMSkill(id)
				}
			}
			else
			{
				client_print(id,print_chat,"[SCXPM] You need one Skillpoint for enhancing your Regeneration.")
			}
		}
		case 3:
		{
			if(skillpoints[id]>0)
			{
#if defined USING_GW
				client_print(id,print_chat,"[SCXPM] Nano Armor skill is currently disabled in GangWars");
#else
				if(rarmor[id]<300)
				{
					if (skillIncrement[id] + rarmor[id] >= 300) {
						skillIncrement[id] = 300 - rarmor[id];
					}
					if (skillpoints[id] >= skillIncrement[id])
					{
						skillpoints[id] -= skillIncrement[id];
						rarmor[id] += skillIncrement[id];
						// insert armorwait code here? - swmpdg
						rarmorwait[id]=rarmor[id]
						client_print(id,print_chat,"[SCXPM] You spent %i Skillpoint to enhance your Nano Armor to Level %i!",skillIncrement[id],rarmor[id]);
					}
					else
					{
						skillIncrement[id] = skillpoints[id]
						skillpoints[id] -= skillIncrement[id];
						rarmor[id] += skillIncrement[id];
						// insert armorwait code here? - swmpdg
						rarmorwait[id]=rarmor[id]
						client_print(id,print_chat,"[SCXPM] You spent %i Skillpoint to enhance your Nano Armor to Level %i!",skillIncrement[id],rarmor[id]);
					}
				}
				else
				{
					client_print(id,print_chat,"[SCXPM] You have mastered your Nano Armor already.")
				}
				if(skillpoints[id]>0)
				{
					SCXPMSkill(id)
				}
#endif
			}
			else
			{
				client_print(id,print_chat,"[SCXPM] You need one Skillpoint for enhancing your Nano Armor.")
			}
		}
		case 4:
		{
			if(skillpoints[id]>0)
			{
				if(rammo[id]<30)
				{
					if (skillIncrement[id] + rammo[id] >= 30) {
						skillIncrement[id] = 30 - rammo[id];
					}
					if (skillpoints[id] >= skillIncrement[id])
					{
						skillpoints[id] -= skillIncrement[id];
						rammo[id] += skillIncrement[id];
						client_print(id,print_chat,"[SCXPM] You spent %i Skillpoints to enhance your Ammo Reincarnation to Level %i!",skillIncrement[id],rammo[id]);
					}
					else
					{
						skillIncrement[id] = skillpoints[id];
						skillpoints[id] -= skillIncrement[id];
						rammo[id] += skillIncrement[id];
						client_print(id,print_chat,"[SCXPM] You spent %i Skillpoints to enhance your Ammo Reincarnation to Level %i!",skillIncrement[id],rammo[id]);

					}
				}
				else
				{
					client_print(id,print_chat,"[SCXPM] You have mastered your Ammo Reincarnation already.")
				}
				if(skillpoints[id]>0)
				{
					SCXPMSkill(id)
				}
			}
			else
			{
				client_print(id,print_chat,"[SCXPM] You need one Skillpoint for enhancing your Ammo Reincarnation.")
			}
		}
		case 5:
		{
			if(skillpoints[id]>0)
			{
				if(gravity[id]<40)
				{
					if (get_pcvar_num(pcvar_gravity) >= 1)
					{
						if (skillIncrement[id] + gravity[id] >= 40) {
							skillIncrement[id] = 40 - gravity[id];
						}
						if (skillpoints[id] >= skillIncrement[id])
						{
							skillpoints[id] -= skillIncrement[id];
							gravity[id] += skillIncrement[id];
							if (is_user_alive(id))
							{
								// set_user_gravity
								gravity_enable(id);
							}
							client_print(id,print_chat,"[SCXPM] You spent %i Skillpoints to enhance your Anti Gravity Device to Level %i!",skillIncrement[id],gravity[id]);
						}
						else
						{
							skillIncrement[id] = skillpoints[id];
							skillpoints[id] -= skillIncrement[id];
							gravity[id] += skillIncrement[id];
							if (is_user_alive(id))
							{
								// set_user_gravity
								gravity_enable(id);
							}
							client_print(id,print_chat,"[SCXPM] You spent %i Skillpoints to enhance your Anti Gravity Device to Level %i!",skillIncrement[id],gravity[id]);
						}
					}
					else
					{
						client_print(id,print_chat,"[SCXPM] Anti Gravity Device is disabled on this map. Please select another skill.")
					}
				}
				else
				{
					client_print(id,print_chat,"[SCXPM] You have mastered your Anti Gravity Device already.")
				}
				if(skillpoints[id]>0)
				{
					SCXPMSkill(id)
				}
			}
			else
			{
				client_print(id,print_chat,"[SCXPM] You need one Skillpoint for enhancing your Anti Gravity Device.")
			}
		}
		case 6:
		{
			if(skillpoints[id]>0)
			{
				if(speed[id]<80)
				{
					if (skillIncrement[id] + speed[id] >= 80) {
						skillIncrement[id] = 80 - speed[id];
					}
					if (skillpoints[id] >= skillIncrement[id])
					{
						skillpoints[id] -= skillIncrement[id];
						speed[id] += skillIncrement[id];
						client_print(id,print_chat,"[SCXPM] You spent %i Skillpoints to enhance your Awareness to Level %i!",skillIncrement[id],speed[id])
					}
					else
					{
						skillIncrement[id] = skillpoints[id];
						skillpoints[id] -= skillIncrement[id];
						speed[id] += skillIncrement[id];
						client_print(id,print_chat,"[SCXPM] You spent %i Skillpoints to enhance your Awareness to Level %i!",skillIncrement[id],speed[id])
					}
				}
				else
				{
					client_print(id,print_chat,"[SCXPM] You have mastered your Awareness already.")
				}
				if(skillpoints[id]>0)
				{
					SCXPMSkill(id)
				}
			}
			else
			{
				client_print(id,print_chat,"[SCXPM] You need one Skillpoint for enhancing your Awareness.")
			}
		}
		case 7:
		{
			if(skillpoints[id]>0)
			{
				if(dist[id]<60)
				{
					if (skillIncrement[id] + dist[id] >= 60) {
						skillIncrement[id] = 60 - dist[id];
					}
					if (skillpoints[id] >= skillIncrement[id])
					{
						skillpoints[id] -= skillIncrement[id];
						dist[id] += skillIncrement[id];
						client_print(id,print_chat,"[SCXPM] You spent %i Skillpoints to enhance your Team Power to Level %i!",skillIncrement[id],dist[id])
					}
					else
					{
						skillIncrement[id] = skillpoints[id];
						skillpoints[id] -= skillIncrement[id];
						dist[id] += skillIncrement[id];
						client_print(id,print_chat,"[SCXPM] You spent %i Skillpoints to enhance your Team Power to Level %i!",skillIncrement[id],dist[id])
					}
				}
				else
				{
					client_print(id,print_chat,"[SCXPM] You have mastered your Team Power already.")
				}
				if(skillpoints[id]>0)
				{
					SCXPMSkill(id)
				}
			}
			else
			{
				client_print(id,print_chat,"[SCXPM] You need one Skillpoint for enhancing your Team Power.")
			}
		}
		case 8:
		{
			if(skillpoints[id]>0)
			{
				if(dodge[id]<90)
				{
					if (skillIncrement[id] + dodge[id] >= 90) {
						skillIncrement[id] = 90 - dodge[id];
					}
					if (skillpoints[id] >= skillIncrement[id])
					{
						skillpoints[id] -= skillIncrement[id];
						dodge[id] += skillIncrement[id];
						client_print(id,print_chat,"[SCXPM] You spent %i Skillpoint to enhance your Dodging and Blocking Skills to Level %i!",skillIncrement[id],dodge[id]);
					}
					else
					{
						skillIncrement[id] = skillpoints[id];
						skillpoints[id] -= skillIncrement[id];
						dodge[id] += skillIncrement[id];
						client_print(id,print_chat,"[SCXPM] You spent %i Skillpoint to enhance your Dodging and Blocking Skills to Level %i!",skillIncrement[id],dodge[id]);

					}
				}
				else
				{
					client_print(id,print_chat,"[SCXPM] You have mastered your Dodging and Blocking Skills already.")
				}
				if(skillpoints[id]>0)
				{
					SCXPMSkill(id)
				}
			}
			else
			{
				client_print(id,print_chat,"[SCXPM] You need one Skillpoint for enhancing your Dodgin and Blocking Skills.")
			}
		}
		case 9:
		{
		}
	}
	return PLUGIN_HANDLED;
}
/* AMXX-Studio Notes - DO NOT MODIFY BELOW HERE
*{\\ rtf1\\ ansi\\ deff0{\\ fonttbl{\\ f0\\ fnil Tahoma;}}\n\\ viewkind4\\ uc1\\ pard\\ lang1031\\ f0\\ fs16 \n\\ par }
*/