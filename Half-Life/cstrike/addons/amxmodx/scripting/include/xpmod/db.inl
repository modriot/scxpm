/*
* The table create code is too long to compile so table have to be created manually
CREATE TABLE IF NOT EXISTS `scxpm_stats` (
  `id` int(11) NOT NULL auto_increment,
  `uniqueid` varchar(50) NOT NULL,
  `authid` varchar(24) NOT NULL,
  `ip` varchar(24) NOT NULL,
  `nick` varchar(50) NOT NULL,
  `xp` bigint(20) NOT NULL default '0',
  `playerlevel` int(11) NOT NULL default '0',
  `skillpoints` int(11) NOT NULL default '0',
  `medals` tinyint(4) NOT NULL default '4',
  `health` int(11) NOT NULL default '0',
  `armor` int(11) NOT NULL default '0',
  `rhealth` int(11) NOT NULL default '0',
  `rarmor` int(11) NOT NULL default '0',
  `rammo` int(11) NOT NULL default '0',
  `gravity` int(11) NOT NULL default '0',
  `speed` int(11) NOT NULL default '0',
  `dist` int(11) NOT NULL default '0',
  `dodge` int(11) NOT NULL default '0',
  `lastUpdated` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `uniqueid` (`uniqueid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
*/
/*
*	Future Features
*	if you want new features ask it on: http://forums.alliedmods.net/showthread.php?t=44168
*	------------------
*	- pruning database with the `lastUpdated` field
*	- cuicide penalty
*
*/
/*
** Queries 
*/
/*
#define QUERY_SELECT_SKILLS "SELECT `xp`, `playerlevel`, `skillpoints`, `medals`, `health`, `armor`, `rhealth`, `rarmor`, `rammo`, `gravity`, `speed`, `dist`, `dodge` FROM `%s` WHERE %s"
#define QUERY_UPDATE_SKILLS "INSERT INTO %s (uniqueid) VALUES ('%s') ON DUPLICATE KEY UPDATE authid ='%s',nick='%s',ip='%s',xp='%d',playerlevel='%d',skillpoints='%d',medals='%d',health='%d',armor='%d',rhealth='%d',rarmor='%d',rammo='%d',gravity='%d',speed='%d',dist='%d',dodge='%d'"
*/
// init the sql, check if the sql table exist
public sql_init() {
	if ( get_pcvar_num( pcvar_save ) >= 2 && !dbc)
	{
		if (get_pcvar_num( pcvar_debug ) == 1 )
		{
			log_amx( "[SCXPM DEBUG] Begin Init the sql" );
		}
#if defined SQLCVAR		
		new host[64], username[64], password[64], dbname[64];
		//no pcvar, only called once at plugin start (now plugin_cfg for Sven Coop)
		get_pcvar_string( pcvar_sql_host, host, 64 );
		get_pcvar_string( pcvar_sql_user, username, 64 );
		get_pcvar_string( pcvar_sql_pass, password, 64 );
		get_pcvar_string( pcvar_sql_db, dbname, 64 );
#endif
		get_pcvar_string( pcvar_sql_table, sql_table, 64 );
		SQL_SetAffinity( "mysql" );
#if defined SQLCVAR
		dbc = SQL_MakeDbTuple( host, username, password, dbname );
#else
		dbc = SQL_MakeDbTuple( DATABASE_HOST, DATABASE_USERNAME, DATABASE_PASSWORD, DATABASE_DATABASE );
#endif
		// check if the table exist
		formatex( g_Cache, 1023, "show tables like '%s'", sql_table );
		SQL_ThreadQuery( dbc, "ShowTableHandle", g_Cache);	
		if (get_pcvar_num( pcvar_debug ) == 1 )
		{
			log_amx( "[SCXPM DEBUG] End Init the sql" );
		}
	}
}
// load empty skills for new player or faulty load
public LoadEmptySkills( id ) {
// add is_user_connected here? may want to use client_authorized afterall ? no... because it is either called before or after client_putinserver. Better to use a task
#if !defined ALLOW_BOTS
	// if(!is_user_bot(id))
	if (is_user_connected(id) && !is_user_bot(id))
#else
	if (is_user_connected(id))
#endif
	{
		xp[id] = 0;
		playerlevel[id] = 0;
		scxpm_calcneedxp( id );
		scxpm_getrank( id );
		skillpoints[id] = 0;
		medals[id] = 4;
		health[id] = 0;
		armor[id] = 0;
		rhealth[id] = 0;
		rarmor[id] = 0;
		rammo[id] = 0;
		gravity[id] = 0;
		speed[id] = 0;
		dist[id] = 0;
		dodge[id] = 0;
		lastfrags[id] = 0;
		firstLvl[id]=0;
#if defined FRAGBONUS
		medal_limit[id]=1;
#endif
#if defined ALLOW_TRADE
		trade_limit[id]=1;
#endif
		spawnmenu[id]=true;
		set_task( 35.0, "scxpm_newbiehelp", id, "", 0, "a", 3 );
	}
}
// save and load
//
// load player data
public LoadPlayerData( id )
{
	new save = get_pcvar_num( pcvar_save );
	new debug_on = get_pcvar_num( pcvar_debug );
	if (plugin_ended == false) 
	{
		if ( debug_on ){
			if(!is_user_bot(id) && !is_user_hltv(id))
			{
			new nickname[35];
			get_user_name( id, nickname, 34 );
			log_amx( "[SCXPM DEBUG] Loading data for: %s", nickname );
			}
		}
		//  CS 1.6 - set to use same system as sven
		// no need to do extra checks if stopping bots from being allowed to pass through from calling function?
#if defined ALLOW_BOTS
		if(!is_user_hltv(id) && is_user_connected(id))
#else
		if(!is_user_bot(id) && !is_user_hltv(id) && is_user_connected(id))
#endif
		{
			// 0 = no saved data
			if ( save <= 0 ){
				LoadEmptySkills( id );
			}else if ( save == 1 ){
				scxpm_loadxp_file( id );
			}else if ( save >= 2 ){
				if ( !dbc ) sql_init();
				scxpm_loadxp_mysql( id );
			}else{
				if ( debug_on ){
					new nickname[35];
					get_user_name( id, nickname, 34 );
					log_amx( "[SCXPM DEBUG] Data already loaded, don't load data for: %s", nickname );
				}
			}
		}
	}
	else {
		if ( debug_on ){
			log_amx( "[SCXPM DEBUG] Plugin already ended, don't load data" );
		}
	}
}
// save player data
public SavePlayerData( id ) {
	new save = get_pcvar_num( pcvar_save );
	new debug_on = get_pcvar_num( pcvar_debug );
	
#if defined ALLOW_BOTS
	if ( save == 0 || is_user_hltv(id) ) return PLUGIN_CONTINUE;
#else
	if ( save == 0 || is_user_bot(id) || is_user_hltv(id) ) return PLUGIN_CONTINUE;
#endif
// are these checks even necessary if setting above returns PLUGIN_CONTINUE? what would PLUGIN_HANDLED do?
#if defined ALLOW_BOTS
	if (plugin_ended == false && !is_user_hltv(id)) {
#else
	if (plugin_ended == false && !is_user_bot(id) && !is_user_hltv(id)) {
#endif
		if (is_player_flag_set(loaddata,id)) {
			if (xp[id] >= 0) {
				//loaddata[id] = false;
				if ( debug_on ){
					new nickname[35];
					get_user_name( id, nickname, 34 );
					log_amx( "[SCXPM DEBUG] Saving data for: %s", nickname ); 
				}
				// 0 = will not be saved
				// 1 = save to file
				// 2 = save to mysql
				if ( save == 1 ){
					scxpm_savexp_file( id );
				}else{
					// why does it need a hltv check here if there is already one above?
					// if ( !dbc && !is_user_hltv(id)) sql_init();
					if ( !dbc )
					{
						sql_init();
					}
					scxpm_savexp_mysql( id );
				}
			}
			else {
				if ( debug_on ){
					new nickname[35];
					get_user_name( id, nickname, 34 );
					log_amx( "[SCXPM DEBUG] xp lower than 0, don't save data for: %s", nickname );
				}
			}
		}
		else {
			if ( debug_on ){
				new nickname[35];
				get_user_name( id, nickname, 34 );
				log_amx( "[SCXPM DEBUG] Data not loaded, don't save data for: %s", nickname );
			}
		}
	}
	else {
		if ( debug_on){
			log_amx( "[SCXPM DEBUG] Plugin already ended, don't save data" );
		}
	}
	return PLUGIN_CONTINUE;
}
// load player data from file
public scxpm_loadxp_file( id ) {
	new savestyle = get_pcvar_num(pcvar_savestyle);
	new authid[35];
	switch(savestyle){
		case 0 : {
			get_user_authid(id, authid, 34)
			if ( containi(authid,"STEAM_ID_PENDING") !=-1 ){
				LoadEmptySkills( id )
				return PLUGIN_CONTINUE;
			}
		}
		case 1 : {
			get_user_ip(id, authid, 34, 1)
		}
		case 2 : {
			get_user_name(id, authid, 34)
		}
	}
	new vaultkey[64], vaultdata[96];
	format(vaultkey,63,"%s-scxpm",authid);
	if ( vaultdata_exists(vaultkey) )
	{
		get_vaultdata(vaultkey,vaultdata,95);
		replace_all(vaultdata,95,"#"," ");
		new pre_xp[16],pre_playerlevel[8],pre_skillpoints[8],pre_medals[8],pre_health[8],pre_armor[8],pre_rhealth[8],pre_rarmor[8],pre_rammo[8],pre_gravity[8],pre_speed[8],pre_dist[8],pre_dodge[8];
		parse(vaultdata,pre_xp,15,pre_playerlevel,7,pre_skillpoints,7,pre_medals,7,pre_health,7,pre_armor,7,pre_rhealth,7,pre_rarmor,7,pre_rammo,7,pre_gravity,7,pre_speed,7,pre_dist,7,pre_dodge,7);
		xp[id] = str_to_num(pre_xp);
		playerlevel[id] = str_to_num(pre_playerlevel);
		scxpm_calcneedxp(id);
		scxpm_getrank(id);
		skillpoints[id] = str_to_num(pre_skillpoints);
		medals[id] = str_to_num(pre_medals);
		health[id] = str_to_num(pre_health);
		armor[id] = str_to_num(pre_armor);
		rhealth[id] = str_to_num(pre_rhealth);
		rarmor[id] = str_to_num(pre_rarmor);
		rammo[id] = str_to_num(pre_rammo);
		gravity[id] = str_to_num(pre_gravity);
		speed[id] = str_to_num(pre_speed);
		dist[id] = str_to_num(pre_dist);
		dodge[id] = str_to_num(pre_dodge);
	}
	else
	{
		log_amx("[SCXPM] Start from level 0");
		LoadEmptySkills( id );
	}
	set_player_flag(loaddata, id);
	return PLUGIN_CONTINUE;
}
// save player data to file
public scxpm_savexp_file( id ) {
	new savestyle = get_pcvar_num(pcvar_savestyle);
	new authid[35];
	switch(savestyle){
		case 0 : {
			get_user_authid( id, authid, 34 );
			if ( containi(authid,"STEAM_ID_PENDING") !=-1 ){
				return PLUGIN_CONTINUE;
			}
		}
		case 1 : {
			get_user_ip( id, authid, 34, 1 );
		}
		case 2 : {
			get_user_name( id, authid, 34 );
		}
	}
	new vaultkey[64], vaultdata[96];
	format( vaultkey, 63, "%s-scxpm", authid );
	format( vaultdata, 95, "%i#%i#%i#%i#%i#%i#%i#%i#%i#%i#%i#%i#%i",xp[id],playerlevel[id],skillpoints[id],medals[id],health[id],armor[id],rhealth[id],rarmor[id],rammo[id],gravity[id],speed[id],dist[id],dodge[id]);
	set_vaultdata( vaultkey, vaultdata );
	return PLUGIN_CONTINUE;
}
// save player data to a database
public scxpm_savexp_mysql( id ) {
	// can add in bot saving by nick here - if(is_user_bot(id) && get_pcvar_num(pcvar_botsave)){save_by_nick(id)} - will do later.
	new savestyle = get_pcvar_num(pcvar_savestyle);
	new authid[35];
	new ip[35];
	new nickname[128];
	new where_statement[1024];
	get_user_authid(id, authid, 34);
	get_user_ip(id, ip, 34, 1);
	get_user_name(id, nickname, 63);
	if ( equali(ip, "") || equali(nickname, "") ) {
		if ( get_pcvar_num( pcvar_debug ) == 1 )
		{
			log_amx( "[SCXPM DEBUG] Empty ip or nickname, don't save data." );
		}
		return PLUGIN_CONTINUE;
	}
	replace_all(nickname,127,"'","\'"); //avoiding sql errors with ' in name
	switch(savestyle){
		case 0 : {
			if ( containi(authid,"STEAM_ID_PENDING") !=-1 || equali(authid, "") ){
				if ( get_pcvar_num( pcvar_debug ) == 1 ){
					log_amx( "[SCXPM DEBUG] Empty or invalid steamid, don't save data." );
				}
				return PLUGIN_CONTINUE;
			}
			formatex( g_Cache, 1023, QUERY_UPDATE_SKILLS, sql_table, authid, authid, nickname, ip, xp[id], playerlevel[id], skillpoints[id], medals[id], health[id], armor[id], rhealth[id], rarmor[id], rammo[id], gravity[id], speed[id], dist[id], dodge[id], where_statement );
		}
		case 1 : {
			formatex( g_Cache, 1023, QUERY_UPDATE_SKILLS, sql_table, ip, authid, nickname, ip, xp[id], playerlevel[id], skillpoints[id], medals[id], health[id], armor[id], rhealth[id], rarmor[id], rammo[id], gravity[id], speed[id], dist[id], dodge[id], where_statement );
		}
		case 2 : {
			formatex( g_Cache, 1023, QUERY_UPDATE_SKILLS, sql_table, nickname, authid, nickname, ip, xp[id], playerlevel[id], skillpoints[id], medals[id], health[id], armor[id], rhealth[id], rarmor[id], rammo[id], gravity[id], speed[id], dist[id], dodge[id], where_statement );
		}
	}
	SQL_ThreadQuery( dbc, "QueryHandle", g_Cache );
	return PLUGIN_CONTINUE;
}
// load player data from a database
public scxpm_loadxp_mysql( id ) {
	new savestyle = get_pcvar_num(pcvar_savestyle);
	if (get_pcvar_num(pcvar_debug) == 1)
	{
		if (!is_user_bot(id) && !is_user_hltv(id))
		{
			log_amx( "[SCXPM DEBUG] Entered public scxpm_loadxp_mysql( id )" );
		}
	}
#if defined ALLOW_BOTS
	if(!is_user_hltv(id) && is_user_connected(id))
#else
	if(!is_user_bot(id) && !is_user_hltv(id) && is_user_connected(id))
#endif
	{
	new where_statement[1024];
	new authid[35];
	new ip[35];
	new nickname[128];
	get_user_authid( id, authid, 34 );
	get_user_ip( id, ip, 34, 1 );
	get_user_name( id, nickname, 63 );
	replace_all(nickname,127,"'","\'"); //avoiding sql errors with ' in name
	switch(savestyle){
		case 0 : {
			if ( containi(authid,"STEAM_ID_PENDING") !=-1 || equali(authid, "") ){
				// this is where the blank data keeps getting loaded... should I just remove this? or make a loop with loadplayerdata?
				set_task(5.0, "LoadEmptySkills", id)
				return PLUGIN_CONTINUE;
			}
			format(where_statement, 199, "`uniqueid` = '%s'", authid);
		}
		case 1 : {
			format(where_statement, 49, "`uniqueid` = '%s'", ip);
		}
		case 2 : {
			format(where_statement, 199, "`uniqueid` = '%s'", nickname);
		}
	}
	formatex( g_Cache, 1023, QUERY_SELECT_SKILLS, sql_table, where_statement);
	new send_id[1];
	send_id[0] = id;
	SQL_ThreadQuery( dbc, "LoadDataHandle", g_Cache, send_id, 1 );
	}
	return PLUGIN_CONTINUE;
}
// sql handles
//
// handle default queries
public QueryHandle( FailState, Handle:Query, Error[], Errcode, Data[], DataSize ) {
	new debug_on = get_pcvar_num( pcvar_debug );
	if (debug_on)	{
		log_amx( "[SCXPM DEBUG] Begin QueryHandle" );
		new sql[1024];
		SQL_GetQueryString ( Query, sql, 1024 );
		log_amx( "[SCXPM DEBUG] executed query: %s", sql );
	}
	// lots of error checking
	if ( FailState == TQUERY_CONNECT_FAILED ) {
		log_amx( "[SCXPM SQL] Could not connect to SQL database." );
		return set_fail_state("[SCXPM SQL] Could not connect to SQL database.");
	}
	else if ( FailState == TQUERY_QUERY_FAILED ) {
		new sql[1024];
		SQL_GetQueryString ( Query, sql, 1024 );
		log_amx( "[SCXPM SQL] SQL Query failed: %s", sql );
		return set_fail_state("[SCXPM SQL] SQL Query failed.");
	}
	if ( Errcode ) {
		return log_amx( "[SCXPM SQL] SQL Error on query: %s", Error );
	}
	if ( debug_on ) {
		log_amx( "[SCXPM DEBUG] End QueryHandle" );
	}
	return PLUGIN_CONTINUE;
}
// check if table exist
public ShowTableHandle(FailState,Handle:Query,Error[],Errcode,Data[],DataSize) {
	new debug_on = get_pcvar_num( pcvar_debug );
	if (debug_on) {
		log_amx("[SCXPM DEBUG] Begin ShowTableHandle");
		new sql[1024];
		SQL_GetQueryString ( Query, sql, 1024 );
		log_amx( "[SCXPM DEBUG] executed query: %s", sql );
	}
	if(FailState==TQUERY_CONNECT_FAILED){
		log_amx( "[SCXPM SQL] Could not connect to SQL database." );
		log_amx( "[SCXPM SQL] Switching to: scxpm_save 0" );
		log_amx( "[SCXPM SQL] Stats won't be saved" );
		set_pcvar_num ( pcvar_save, 0 );
		return PLUGIN_CONTINUE;
	}
	else if (FailState == TQUERY_QUERY_FAILED) {
		log_amx( "[SCXPM SQL] Query failed." );
		log_amx( "[SCXPM SQL] Switching to: scxpm_save 0" );
		log_amx( "[SCXPM SQL] Stats won't be saved" );
		set_pcvar_num ( pcvar_save, 0 );
		return PLUGIN_CONTINUE;
	}
	if (Errcode) {
		log_amx( "[SCXPM SQL] Error on query: %s", Error );
		log_amx( "[SCXPM SQL] Switching to: scxpm_save 0" );
		log_amx( "[SCXPM SQL] Stats won't be saved" );
		set_pcvar_num ( pcvar_save, 0 );
		return PLUGIN_CONTINUE;
	}
	if (SQL_NumResults(Query) > 0) {
		if (get_pcvar_num( pcvar_debug ) == 1 )
		{
			log_amx( "[SCXPM DEBUG] Database table found: %s", sql_table );
		}
	}
	else {
		log_amx( "[SCXPM SQL] Could not find the table: %s", sql_table );
		log_amx( "[SCXPM SQL] Switching to: scxpm_save 0" );
		log_amx( "[SCXPM SQL] Stats won't be saved" );
		set_pcvar_num ( pcvar_save, 0 );
	}
	if (debug_on){
		log_amx( "[SCXPM DEBUG] End ShowTableHandle" );
	}
	return PLUGIN_CONTINUE;
}
// load player data
public LoadDataHandle(FailState,Handle:Query,Error[],Errcode,Data[],DataSize) {
	new debug_on = get_pcvar_num( pcvar_debug );
	if (debug_on){
		log_amx( "[SCXPM DEBUG] Begin LoadDataHandle" );
		new sql[1024];
		SQL_GetQueryString ( Query, sql, 1024 );
		log_amx( "[SCXPM DEBUG] executed query: %s", sql );
	}
	if (FailState == TQUERY_CONNECT_FAILED) {
		return set_fail_state("Could not connect to SQL database.")
	}else if (FailState == TQUERY_QUERY_FAILED){
		return set_fail_state("Query failed.")
	}
	if (Errcode){
		return log_amx("Error on query: %s",Error)
	}
	new id = Data[0];
	set_player_flag(loaddata, id);
	if (SQL_NumResults(Query) >= 1) {
		if (SQL_NumResults(Query) > 1) {
			if (get_pcvar_num( pcvar_debug ) == 1 ) {
				log_amx( "[SCXPM DEBUG] more than one entry found. just take the first one" );
			}
		}
		xp[id] = SQL_ReadResult(Query, 0) ;
		playerlevel[id] = SQL_ReadResult(Query, 1);
		scxpm_calcneedxp( id );
		scxpm_getrank( id );
		skillpoints[id] = SQL_ReadResult( Query, 2 );
		medals[id] = SQL_ReadResult(Query, 3);
		health[id] = SQL_ReadResult(Query, 4);
		armor[id] = SQL_ReadResult(Query, 5);
		rhealth[id] = SQL_ReadResult(Query, 6); 
		rarmor[id] = SQL_ReadResult(Query, 7);
		rammo[id] = SQL_ReadResult(Query, 8);
		gravity[id] = SQL_ReadResult(Query, 9); 
		speed[id] = SQL_ReadResult(Query, 10);
		dist[id] = SQL_ReadResult(Query, 11);
		dodge[id] = SQL_ReadResult(Query, 12);
		// need to use this for cs as well if I want hp to load on first player spawn quickly
		set_task(1.0, "load_hpap", id)
		set_task(1.5, "gravity_msg", id)
	}
	else if (SQL_NumResults(Query) < 1){
		// load empty skills, the skills will be saved later on
		// is this even necessary? wtf... may need to put loop here to LoadPlayerData too?
		// cs needs this now for same style of mysql saving and loading..

		set_task(1.0, "LoadEmptySkills", id)
		set_task(1.5, "gravity_msg", id)
	}
	if (debug_on){
		log_amx( "[SCXPM DEBUG] End LoadDataHandle" );
	}
	return PLUGIN_CONTINUE;
}
/* AMXX-Studio Notes - DO NOT MODIFY BELOW HERE
*{\\ rtf1\\ ansi\\ deff0{\\ fonttbl{\\ f0\\ fnil Tahoma;}}\n\\ viewkind4\\ uc1\\ pard\\ lang1031\\ f0\\ fs16 \n\\ par }
*/