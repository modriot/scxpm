// Inline file configuration by Swamp Dog - May 2016

// define to use Testing Team Power
// #define TEST_TP

// no hltv, no bots - configured by swmpdg. Will be removing teampower. Changed all [33] values to [32] (incorrect?)
// comment the #define USING_CS if you don't use Counter-Strike
//#define USING_CS

// comment the #define USING_GW if you don't use GangWars
#define USING_GW

// added for Sven Coop specific loading - comment when using with other games/mods
//#define USING_SVEN

// comment the #define ALLOW_BOTS if you don't want bots to be allowed to gain skills, gain xp, or save xp/skills - also to save resources (less checks for bots, usage in svencoop) swmpdg
// I need to add bot checks for when client connects and such - for use with CS and other servers
//#define ALLOW_BOTS

#if !defined USING_GW
// comment the #define ARMOR_TP to turn off Team Power Armor - swmpdg
#define ARMOR_TP

#endif

// comment to remove damage event code
#define EVENT_DMG

// comment to remove Ammo Regeneration Team Power functionality
#define AMMO_TP

// comment to remove Speed Control design by swmpdg - Counter-Strike seems to need it disabled - only useful for trying to stop speed running in Sven Coop?
// will need to use differently for CS...for now, turn off.
#define SPEED_CTRL

// comment to remove Gravity Damage control (player is set with high gravity if attacked)
#define GRAV_DMG

// comment to remove monstermod + random monsters fun support (needs ham sandwich, likely unstable with sven coop)
//#define ALLOW_MONSTERS

// comment to use hard-coded defined SQL DB info
#define SQLCVAR

// comment to remove Bot Event registry (register_event ResetHUD and SetFOV)
// #define ALLOW_BOT_EVENT

// comment to remove "Frag Bonus" and winning medals
#define FRAGBONUS

// comment to disallow trading of XP / Medals
#define ALLOW_TRADE

// comment to remove count-control for showdata and reexp - make count control pcvars? - turn off for cs? may want to use in cs...
#define COUNT_CTRL

// comment to remove check for if player is above level 0 to run code
#define PLYR_CTRL

#if defined USING_CS
#include <cstrike>
#endif

#if defined USING_GW
#include "gangwars_const.inc"
#endif

#if defined ALLOW_MONSTERS
#include <hamsandwich>
#endif

#define VERSION "17.31.24"
#define LASTUPDATE "16 May 2016"

#if !defined SQLCVAR

#define DATABASE_HOST "127.0.0.1"
#define DATABASE_USERNAME "root"
#define DATABASE_PASSWORD "password"
#define DATABASE_DATABASE "database_name"

#endif
/*
*	Future Features
*	if you want new features ask it on: http://forums.alliedmods.net/showthread.php?t=44168
*	------------------
*	- pruning database with the `lastUpdated` field
*	- cuicide penalty
*
*/
/*
** Queries 
*/
#define QUERY_SELECT_SKILLS "SELECT `xp`, `playerlevel`, `skillpoints`, `medals`, `health`, `armor`, `rhealth`, `rarmor`, `rammo`, `gravity`, `speed`, `dist`, `dodge` FROM `%s` WHERE %s"
#define QUERY_UPDATE_SKILLS "INSERT INTO %s (uniqueid) VALUES ('%s') ON DUPLICATE KEY UPDATE authid ='%s',nick='%s',ip='%s',xp='%d',playerlevel='%d',skillpoints='%d',medals='%d',health='%d',armor='%d',rhealth='%d',rarmor='%d',rammo='%d',gravity='%d',speed='%d',dist='%d',dodge='%d'"

// added for mysql support
new Handle:dbc;
new g_Cache[1024];
new sql_table[64];
new loaddata;		// check so data is only saved when data is loaded
new bool:plugin_ended;
#if defined USING_CS
// cs specific
new ammotype[7][15];		// types of ammo you can receive through random ammo (from regeneration)
#endif
#if defined USING_GW
// gw specific
new ammotype[6][15];		// types of ammo you can receive through random ammo (from regeneration)
#endif
new const ranks[23][] = {
	"Frightened Civilian",
	"Civilian",
	"Fighter",
	"Private Third Class",
	"Private Second Class",
	"Private First Class",
	"Free Agent",
	"Professional Free Agent",
	"Professional Force Member",
	"Professional Force Leader",
	"Special Force Member",
	"Special Force Leader",
	"United Forces Member",
	"United Forces Leader",
	"Hidden Operations Member",
	"Hidden Operations Scheduler",
	"Hidden Operations Leader",
	"General",
	"Top 30 of most famous Leaders",
	"Top 15 of most famous Leaders",
	"Highest Force Member",
	"Highest Force Leader",
	"Loading..."
}
new xp[33];
new neededxp[33];
new playerlevel[33];
new rank[33];
new skillpoints[33];
new medals[35];
new health[33];
new armor[33];
new rhealth[33];
new rarmor[33];
new rammo[33];
new gravity[33];
new speed[33];
new dist[33];
new dodge[33];
new rarmorwait[33];
new rhealthwait[33];
new ammowait[33];
new lastfrags[33];
new firstLvl[33];
#if defined COUNT_CTRL
new count_reexp[33];
new count_save[33];
new maxlvl_count[33];
#endif
new skillIncrement[33];
#if defined FRAGBONUS
new medal_limit[33];
#endif
#if defined ALLOW_TRADE
new trade_limit[33];
#endif
new iPlayers[32];
new has_godmode, iNum;
#if !defined USING_SVEN
new pcvar_gamename, pcvar_save, pcvar_debug, pcvar_savestyle, pcvar_minplaytime;
#else
new pcvar_save, pcvar_debug, pcvar_savestyle, pcvar_minplaytime;
#endif
#if !defined SQLCVAR
new pcvar_sql_table;
#else
new pcvar_sql_host, pcvar_sql_user, pcvar_sql_pass, pcvar_sql_db, pcvar_sql_table;
#endif
new pcvar_hud_channel, pcvar_save_frequent, pcvar_xpgain, pcvar_maxlevel;
#if !defined USING_CS
#if !defined USING_GW
new pcvar_maxlevelup_enabled,pcvar_maxlevelup,pcvar_maxlevelup_limit;
#endif
#endif
#if defined FRAGBONUS
new pcvar_gravity, pcvar_fraglimit, pcvar_bonus;
#else
new pcvar_gravity;
#endif
#if defined COUNT_CTRL
#if defined USING_CS
new pcvar_counter;
#else
#if defined USING_GW
new pcvar_counter;
#endif
#if !defined USING_GW
new pcvar_counter, pcvar_savectrl;
#endif
#endif
#if defined EVENT_DMG
//thx to jonnyboy0719 for this idea and code from his RPG Mod
// Booleans
new bool:PlayerIsHurt[33] = false;
#endif
#if defined PLYR_CTRL
new bool:onecount[33] = false; // used for level 0 player loading or waiting to gain xp in sc_reexp.. need to remove
#endif
new bool:spawnmenu[33] = false;
#if !defined USING_GW
new health_values, armor_values, distanceRange, luck;
#else
new health_values, distanceRange, luck;
#endif
new fragcount, resetfrags, fragbonus;
#if defined SPEED_CTRL
new bool:g_hasSpeed[33] = false;
new bool:g_punished[33] = false;
new pcvar_speedctrl, pcvar_reduce
#endif
#if defined ALLOW_MONSTERS
new AgrXP, ApaXP, BarXP, BigXP, BulXP, ConXP, GarXP, HeaXP, HouXP, HasXP, HgrXP, SciXP, IslXP, SnaXP, ZomXP
#endif