// give ammo
// I am thinking about changing this area as well, to include weapons that normally do not spawn or give ammo such as tripmines and grenades and satchels - and I did it...
public scxpm_randomammo( id ) {
#if !defined USING_GW
	new number = random_num(0,6);
#else
	new number = random_num(0,5);
#endif
#if defined USING_CS
	give_item(id, ammotype[number]);
#endif
#if defined USING_GW
	give_item(id, ammotype[number]);
#endif
#if !defined USING_CS
#if !defined USING_GW
#if defined USING_SVEN
	new clip,ammo
	if(number==0)
	{
		get_user_ammo(id,2,clip,ammo)
		if(ammo<250)
		{
			give_item(id,"ammo_9mmAR")
			give_item(id,"ammo_9mmAR")
		}
		else
		{
			number=1
		}
	}
	if(number==1)
	{
		get_user_ammo(id,3,clip,ammo)
		if(ammo<36)
		{
			give_item(id,"ammo_357")
			give_item(id,"ammo_357")
			give_item(id,"ammo_357")
		}
		else
		{
			number=2
		}
	}
	if(number==2)
	{
		get_user_ammo(id,7,clip,ammo)
		if(ammo<125)
		{
			give_item(id,"ammo_buckshot")
			give_item(id,"ammo_buckshot")
			give_item(id,"ammo_buckshot")
		}
		else
		{
			number=3
		}
	}
	if(number==3)
	{
		get_user_ammo(id,9,clip,ammo)
		if(ammo<100)
		{
			give_item(id,"ammo_gaussclip")
			give_item(id,"ammo_gaussclip")
		}
		else
		{
			number=4
		}
	}
	if(number==4)
	{
		get_user_ammo(id,6,clip,ammo)
		if(ammo<50)
		{
			give_item(id,"ammo_crossbow")
			give_item(id,"ammo_crossbow")
		}
		else
		{
			number=5
		}
	}
	if(number==5)
	{
		get_user_ammo(id,8,clip,ammo)
		if(ammo<5)
		{
			give_item(id,"ammo_rpgclip")
		}
		else
		{
			number=6
		}
	}
	if(number==6)
	{
		get_user_ammo(id,23,clip,ammo)
		if(ammo<15)
		{
			give_item(id,"ammo_762")
			give_item(id,"ammo_762")
			give_item(id,"ammo_556")
			give_item(id,"ammo_556")
			give_item(id,"ammo_sporeclip")
			give_item(id,"ammo_sporeclip")
			give_item(id,"ammo_sporeclip")
			give_item(id,"ammo_sporeclip")
			give_item(id,"ammo_sporeclip")
		}
	}
#endif
#endif
#endif
}
// whole code being done to remove "i" and replace with "id" (except in team power, changed id to i)
// give stuff
public scxpm_regen(id) {
// the bot check may not be needed if bots are stopped from loading this code in scxpm_sdac in client_putinserver with a bot check...
// may still need "(is_user_connected(id))".. needs check?
	if(is_user_alive(id))
	{
#if defined PLYR_CTRL
		if(playerlevel[id]>0)
		{
#endif
#if defined EVENT_DMG
			if (!PlayerIsHurt[id])
			{
#endif
				if(rhealth[id]>0)
				{
					if (rhealthwait[id]>=380)
					{
						if(get_user_health(id)<health[id]+100+(medals[id]-1)+speed[id])
						{
							set_user_health(id,get_user_health(id)+1)
							rhealthwait[id]=rhealth[id]+speed[id]+medals[id]
							
						}
					}
					else
					{
						rhealthwait[id]+=rhealth[id]+speed[id]+medals[id]
					}
				}
#if !defined USING_GW
				if(rarmor[id]>0)
				{
					if(rarmorwait[id]>=380)
					{

						if(get_user_armor(id)<100+armor[id]+(medals[id]-1)+speed[id])
						{
#if defined USING_CS
							cs_set_user_armor(id,get_user_armor(id)+1, CS_ARMOR_VESTHELM)
#else
							set_user_armor(id,get_user_armor(id)+1)
#endif
							rarmorwait[id]=rarmor[id]+speed[id]+medals[id]
						}
					}
					else
					{
						rarmorwait[id]+=rarmor[id]+speed[id]+medals[id]
					}
				}
#endif
				// idea for ammo regen - why... should there be random ammo if a player does not have a weapon??? why ??? ... could reduce load of game greatly if removing random ammo
				if(rammo[id]>0)
				{
					if (ammowait[id]>=1000)
					{
#if defined USING_GW
						
						// support for reading weapons for ammo added by swmpdg - may 07 2016
						// Gangwars support added - may need to make CONST file
						new clip,ammo
						switch(get_user_weapon(id,clip,ammo))
						{
							case 1: /* BERETTA */
							{
								
								get_user_ammo(id,1,clip,ammo)
								if(ammo<30)
								{
									give_item(id,"ammo_357sig")
								}
								else
								{
									scxpm_randomammo(id)
								}
								//scxpm_randomammo(id)
							}
							case 2: /* GLOCK18 */
							{
								
								get_user_ammo(id,2,clip,ammo)
								if(ammo<42)
								{
									give_item(id,"ammo_9mm")
								}
								else
								{
									scxpm_randomammo(id)
								}
								
								//scxpm_randomammo(id)
							}
							// no weapon 2 index in counter-strike
							case 3: /* DEAGLE */
							{					
								get_user_ammo(id,3,clip,ammo)
								if(ammo<30)
								{
									give_item(id,"ammo_50ae")
								}
								else
								{
									scxpm_randomammo(id)
								}
							}
							case 4: /* TMP */
							{
								get_user_ammo(id,4,clip,ammo)
								if(ammo<90)
								{
									give_item(id,"ammo_9mm")
								}
								else
								{
									scxpm_randomammo(id)
								}
							}
							case 5: /* MAC10 */
							{
								get_user_ammo(id,5,clip,ammo)
								if(ammo<100)
								{
									give_item(id,"ammo_9mm")
								}
								else
								{
									scxpm_randomammo(id)
								}
							}
							case 6: /* UMP45 */
							{
								get_user_ammo(id,6,clip,ammo)
								if(ammo<100)
								{
									give_item(id,"ammo_9mm")
								}
								else
								{
									scxpm_randomammo(id)
								}
							}
							case 7: /* MP5NAVY */
							{
								get_user_ammo(id,7,clip,ammo)
								if(ammo<120)
								{
									give_item(id,"ammo_9mm")
								}
								else
								{
									scxpm_randomammo(id)
								}
							}
							case 8: /* AK-74 */
							{
								get_user_ammo(id,8,clip,ammo)
								if(ammo<90)
								{
									give_item(id,"ammo_762nato")
								}
								else
								{
									scxpm_randomammo(id)
								}
							}
							case 9: /* M16 */
							{
								get_user_ammo(id,9,clip,ammo)
								if(ammo<90)
								{
									give_item(id,"ammo_556nato")
								}
								else
								{
									scxpm_randomammo(id)
								}
							}
							case 10: /* SG552 (AK-47) */
							{
								get_user_ammo(id,10,clip,ammo)
								if(ammo<90)
								{
									give_item(id,"ammo_556nato")
								}
								else
								{
									scxpm_randomammo(id)
								}
							}
							case 11: /* AUG (KA DR200) */
							{
								get_user_ammo(id,11,clip,ammo)
								if(ammo<120)
								{
									give_item(id,"ammo_762nato")
								}
								else
								{
									scxpm_randomammo(id)
								}
							}
							case 12: /* SG550 (H&K PSG-1) */
							{
								get_user_ammo(id,12,clip,ammo)
								if(ammo<120)
								{
									give_item(id,"ammo_762nato")
								}
								else
								{
									scxpm_randomammo(id)
								}
							}
							case 13: /* M3 (MOSSBERG) */
							{
								get_user_ammo(id,13,clip,ammo)
								if(ammo<40)
								{
									give_item(id,"ammo_buckshot")
								}
								else
								{
									scxpm_randomammo(id)
								}
							}
							case 14: /* XM1014 (BENELLI) */ 
							{
								get_user_ammo(id,14,clip,ammo)
								if(ammo<49)
								{
									give_item(id,"ammo_buckshot")
								}
								else
								{
									scxpm_randomammo(id)
								}
							}
							case 30: /* HE_GRENADE */
							{
								scxpm_randomammo(id)
							}
							case 31: /* KNIFE */
							{
								scxpm_randomammo(id)
							}
						}
#endif
#if defined USING_CS
						
						// support for reading weapons for ammo added by swmpdg - may 07 2016
						new clip,ammo
						switch(get_user_weapon(id,clip,ammo))
						{
							case 1: /* P228 */
							{
								get_user_ammo(id,21,clip,ammo)
								if(ammo<rammo[id]+13)
								{
									give_item(id,"ammo_357sig")
								}
								else
								{
									scxpm_randomammo(id)
								}
							}
							// no weapon 2 index in counter-strike
							case 3: /* SCOUT */
							{							
								get_user_ammo(id,3,clip,ammo)
								if(ammo<20)
								{
									give_item(id,"ammo_762nato")
								}
								else
								{
									scxpm_randomammo(id)
								}									
							}
							case 4: /* HEGRENADE */
							{
								scxpm_randomammo(id)
							}
							case 5: /* XM1014 */
							{
								get_user_ammo(id,5,clip,ammo)
								if(ammo<21)
								{
									give_item(id,"ammo_buckshot")
								}
								else
								{
									scxpm_randomammo(id)
								}
							}
							case 6: /* C4 */
							{
								scxpm_randomammo(id)
							}
							case 7: /* MAC10 */
							{
								get_user_ammo(id,7,clip,ammo)
								if(ammo<60+rammo[id])
								{
									give_item(id,"ammo_45acp")
								}
								else
								{
									scxpm_randomammo(id)
								}
							}
							case 8: /* AUG */
							{
								get_user_ammo(id,8,clip,ammo)
								if(ammo<60+rammo[id])
								{
									give_item(id,"ammo_556nato")
								}
								else
								{
									scxpm_randomammo(id)
								}
							}
							case 9: /* SMOKEGRENADE */
							{
								scxpm_randomammo(id)
							}
							case 10: /* ELITES */
							{
								get_user_ammo(id,10,clip,ammo)
								if(ammo<100)
								{
									give_item(id,"ammo_9mm")
								}
								else
								{
									scxpm_randomammo(id)
								}
							}
							case 11: /* FIVESEVEN */
							{
								get_user_ammo(id,11,clip,ammo)
								if(ammo<60+rammo[id])
								{
									give_item(id,"ammo_57mm")
								}
								else
								{
									scxpm_randomammo(id)
								}
							}
							case 12: /* UMP45 */
							{
								get_user_ammo(id,12,clip,ammo)
								if (ammo<60+rammo[id])
								{
									give_item(id, "ammo_45acp")
									give_item(id, "ammo_45acp")
								}
								else
								{
									scxpm_randomammo(id)
								}
							}
							case 13: /* SG550 */
							{
								get_user_ammo(id,13,clip,ammo)
								if (ammo<60+rammo[id])
								{
									give_item(id, "ammo_556nato")
								}
								else
								{
									scxpm_randomammo(id)
								}
							}
							case 14: /* GALIL */ 
							{
								get_user_ammo(id,14,clip,ammo)
								if (ammo<60+rammo[id])
								{
									give_item(id, "ammo_556nato")
								}
								else
								{
									scxpm_randomammo(id)
								}
							}
							case 15: /* FAMAS */ 
							{
								get_user_ammo(id,15,clip,ammo)
								if (ammo<60+rammo[id])
								{
									give_item(id, "ammo_556nato")
								}
								else
								{
									scxpm_randomammo(id)
								}
							}
							case 16: /* USP */
							{
								get_user_ammo(id,16,clip,ammo)
								if(ammo<250)
								{
									give_item(id,"ammo_45acp")
								}
								else
								{
									scxpm_randomammo(id)
								}
							}
							case 17: /* GLOCK18 */
							{
								get_user_ammo(id,17,clip,ammo)
								if(ammo<60+rammo[id])
								{
									give_item(id,"ammo_9mm")
								}
								else
								{
									scxpm_randomammo(id)
								}
							}
							case 18: /* AWP */
							{
								get_user_ammo(id,18,clip,ammo)
								if(ammo<20)
								{
									give_item(id,"ammo_338magnum")
								}
								else
								{
									scxpm_randomammo(id)
								}
							}
							case 19: /* MP5NAVY */
							{
								get_user_ammo(id,19,clip,ammo)
								if(ammo<60+rammo[id])
								{
									give_item(id,"ammo_9mm")
								}
								else
								{
									scxpm_randomammo(id)
								}
							}
							case 20: /* M249 */
							{
								get_user_ammo(id,20,clip,ammo)
								if(ammo<100+rammo[id])
								{
									give_item(id,"ammo_556natobox")
								}
								else
								{
									scxpm_randomammo(id)
								}
							}
							case 21: /* M3 */
							{
								get_user_ammo(id,21,clip,ammo)
								if(ammo<24)
								{
									give_item(id,"ammo_buckshot")
								}
								else
								{
									scxpm_randomammo(id)
								}
							}
							case 22: /* M4A1 */
							{
								get_user_ammo(id,22,clip,ammo)
								if(ammo<60+rammo[id])
								{
									give_item(id,"ammo_556nato")
								}
								else
								{
									scxpm_randomammo(id)
								}
							}
							case 23: /* TMP */
							{
								// can go up to 120...
								get_user_ammo(id,23,clip,ammo)
								if(ammo<60+rammo[id])
								{
									give_item(id,"ammo_9mm")
									give_item(id,"ammo_9mm")
								}
								else
								{
									scxpm_randomammo(id)
								}
							}
							case 24: /* G3SG1 */
							{

								get_user_ammo(id,24,clip,ammo)
								if(ammo<60+rammo[id])
								{
									give_item(id,"ammo_762nato")
								}
								else
								{
									scxpm_randomammo(id)
								}
							}
							case 25: /* FLASHBANG */
							{
								scxpm_randomammo(id)
							}
							case 26: /* DEAGLE */
							{
								get_user_ammo(id,26,clip,ammo)
								if(ammo<21)
								{
									give_item(id,"ammo_50ae")
								}
								else
								{
									scxpm_randomammo(id)
								}
							}
							case 27: /* SG552 */
							{
								get_user_ammo(id,27,clip,ammo)
								if(ammo<60+rammo[id])
								{
									give_item(id,"ammo_556nato")
								}
								else
								{
									scxpm_randomammo(id)
								}
							}
							case 28: /* AK47 */
							{
								get_user_ammo(id,27,clip,ammo)
								if (ammo<60+rammo[id])
								{
									give_item(id,"ammo_762nato")	
								}
								else
								{
									scxpm_randomammo(id)
								}
								
							}
							case 29: /* KNIFE */
							{
								scxpm_randomammo(id)
							}
							case 30: /* P90 */
							{
								get_user_ammo(id,30,clip,ammo)
								if (ammo<60+rammo[id])
								{
									give_item(id,"ammo_57mm")
								}
								else
								{
									scxpm_randomammo(id)
								}
							}
						}
#endif
#if defined USING_SVEN
						new clip,ammo
						switch(get_user_weapon(id,clip,ammo))
						{
							case 1: /* Crowbar */
							{
								scxpm_randomammo(id)
							}
							case 2: /* 9mm Handgun */
							{
								get_user_ammo(id,2,clip,ammo)
								if(ammo<250)
								{
									give_item(id,"ammo_9mmAR")
								}
								else
								{
									scxpm_randomammo(id)
								}
							}
							case 3: /* 357 (Revolver) */
							{
								get_user_ammo(id,3,clip,ammo)
								if(ammo<36)
								{
									give_item(id,"ammo_357")
									give_item(id,"ammo_357")
								}
								else
								{
									scxpm_randomammo(id)
								}
							}
							case 4: /* 9mm AR = MP5 */
							{
								get_user_ammo(id,4,clip,ammo)
								if(ammo<250)
								{
									give_item(id,"ammo_9mmAR")
									give_item(id,"ammo_9mmAR")
								}
								else
								{
									scxpm_randomammo(id)
								}
							}
							case 6: /* Crossbow */
							{
								get_user_ammo(id,6,clip,ammo)
								if(ammo<50)
								{
									give_item(id,"ammo_crossbow")
									give_item(id,"ammo_crossbow")
								}
								else
								{
									scxpm_randomammo(id)
								}
							}
							case 7: /* Shotgun */
							{
								get_user_ammo(id,7,clip,ammo)
								if(ammo<125)
								{
									give_item(id,"ammo_buckshot")
									give_item(id,"ammo_buckshot")
								}
								else
								{
									scxpm_randomammo(id)
								}
							}
							case 8: /* RPG Launcher */
							{
								get_user_ammo(id,8,clip,ammo)
								if(ammo<5)
								{
									give_item(id,"ammo_rpgclip")
								}
								else
								{
									scxpm_randomammo(id)
								}
							}
							case 9: /* Gauss Cannon */
							{
								get_user_ammo(id,9,clip,ammo)
								if(ammo<100)
								{
									give_item(id,"ammo_gaussclip")
									give_item(id,"ammo_gaussclip")
								}
								else
								{
									scxpm_randomammo(id)
								}
							}
							case 10: /* Egon */
							{
								get_user_ammo(id,10,clip,ammo)
								if(ammo<100)
								{
									give_item(id,"ammo_gaussclip")
									give_item(id,"ammo_gaussclip")
								}
								else
								{
									scxpm_randomammo(id)
								}
							}
							case 11: /* Hornetgun */
							{
								scxpm_randomammo(id)
							}
							case 12: /* Handgrenade */
							{
								/*
								get_user_ammo(id,12,clip,ammo)
								if (ammo<10)
								{
									// gives 5 nades at a time...may be over-powered and may need a limit
									give_item(id, "weapon_handgrenade")
								}
								else
								{
									scxpm_randomammo(id)
								}
								*/
								scxpm_randomammo(id)
							}
							case 13: /* Tripmine */
							{
								/*
								get_user_ammo(id,13,clip,ammo)
								if (ammo<5)
								{
									give_item(id, "weapon_tripmine")
								}
								*/
								scxpm_randomammo(id)
							}
							case 14: /* Satchels */ // fixed, was 13
							{
								/*
								get_user_ammo(id,14,clip,ammo)
								if (ammo<5)
								{
									give_item(id, "weapon_satchel")
								}
								else
								{
									scxpm_randomammo(id)
								}
								*/
								scxpm_randomammo(id)
							}
							case 15: /* Snarks */ // fixed, was 13
							{
								/*
								get_user_ammo(id,15,clip,ammo)
								if (ammo<15)
								{
									give_item(id, "weapon_snark")
								}
								else
								{
									scxpm_randomammo(id)
								}
								*/
								scxpm_randomammo(id)
							}
							case 16: /* Uzi Akimbo */
							{
								get_user_ammo(id,16,clip,ammo)
								if(ammo<250)
								{
									give_item(id,"ammo_9mmAR")
									give_item(id,"ammo_9mmAR")
								}
								else
								{
									scxpm_randomammo(id)
								}
							}
							case 17: /* Uzi */
							{
								get_user_ammo(id,17,clip,ammo)
								if(ammo<100)
								{
									give_item(id,"ammo_9mmAR")
									give_item(id,"ammo_9mmAR")
								}
								else
								{
									scxpm_randomammo(id)
								}
							}
							case 18: /* Medkit */
							{
								scxpm_randomammo(id)
							}
							case 20: /* Pipewrench */
							{
								scxpm_randomammo(id)
							}
							case 21: /* Minigun */
							{
								get_user_ammo(id,21,clip,ammo)
								if(ammo<100)
								{
									give_item(id,"ammo_556")
									give_item(id,"ammo_556")
								}
								else
								{
									scxpm_randomammo(id)
								}
							}
							case 22: /* Grapple */
							{
								scxpm_randomammo(id)
							}
							case 23: /* Sniper Rifle */
							{
								get_user_ammo(id,23,clip,ammo)
								if(ammo<15)
								{
									give_item(id,"ammo_762")
									give_item(id,"ammo_762")
								}
								else
								{
									scxpm_randomammo(id)
								}
							}
							case 24: /* M249 Saw */
							{

								get_user_ammo(id,24,clip,ammo)
								//if(ammo<600)
								if(ammo<100)
								{
									give_item(id,"ammo_556")
									give_item(id,"ammo_556")
								}
								else
								{
									scxpm_randomammo(id)
								}
							}
							case 25: /* M16 */
							{
								get_user_ammo(id,25,clip,ammo)
								if(ammo<600)
								{
									give_item(id,"ammo_556")
									give_item(id,"ammo_ARgrenades")
								}
								else
								{
									scxpm_randomammo(id)
								}
							}
							case 26: /* Spore Launcher */
							{
								get_user_ammo(id,26,clip,ammo)
								if(ammo<30)
								{
									give_item(id,"ammo_sporeclip")
									give_item(id,"ammo_sporeclip")
									give_item(id,"ammo_sporeclip")
									give_item(id,"ammo_sporeclip")
									give_item(id,"ammo_sporeclip")
									give_item(id,"ammo_sporeclip")
									give_item(id,"ammo_sporeclip")
									give_item(id,"ammo_sporeclip")
									give_item(id,"ammo_sporeclip")
									give_item(id,"ammo_sporeclip")
								}
								else
								{
									scxpm_randomammo(id)
								}
							}
							case 27: /* Desert Eagle */
							{
								get_user_ammo(id,27,clip,ammo)
								if(ammo<36)
								{
									give_item(id,"ammo_357")
									give_item(id,"ammo_357")
									give_item(id,"ammo_357")
								}
								else
								{
									scxpm_randomammo(id)
								}
							}
							case 28: /* Shock Roach */
							{
								// removed battery, because it "falls through map" on Escape Series...??
								scxpm_randomammo(id)
							}
						}
#endif
						ammowait[id]=rammo[id]
					}
					else
					{
						ammowait[id]+=rammo[id]+speed[id]+medals[id]
					}
				}
#if !defined USING_CS
#if !defined USING_GW
				new clip,ammo
				switch(get_user_weapon(id,clip,ammo))
				{
					case 1: /* Crowbar */
					{
						if(rarmor[id] > 0)
						{
							if (get_user_armor(id) < 100)
							{
								set_user_armor(id, get_user_armor(id)+1)
							}
							else if (get_user_armor(id) < 100+armor[id]+(medals[id]-1)+speed[id])
							{
								rarmorwait[id]+=medals[id]+speed[id]+rarmor[id]
							}
						}
					}
					case 18: /* Medkit */
					{
						if(rhealth[id] > 0)
						{
							if(get_user_health(id)<100+health[id])
							{
								set_user_health(id,get_user_health(id)+1)
							}
							else if(get_user_health(id)<health[id]+100+(medals[id]-1)+speed[id])
							{
								rhealthwait[id]+=medals[id]+speed[id]+rhealth[id]
							}
						}
					}
					case 20: /* Pipewrench */
					{
						if(rarmor[id] > 0)
						{
							if (get_user_armor(id) < 100)
							{
								set_user_armor(id, get_user_armor(id)+1)
							}
							else if (get_user_armor(id) < 100+armor[id]+(medals[id]-1)+speed[id])
							{
								rarmorwait[id]+=medals[id]+speed[id]+rarmor[id]
							}
						}
					}
				}

				if(dist[id]>0)
				{
					get_players(iPlayers,iNum)
#if defined TEST_TP
					if(iNum>=1) // for testing puroses without someone else in server
#else
					if(iNum>1)
#endif
					{
						for(new h=0;h<iNum+1;h++)
						// for(new h=0;h<iNum;h++)
						// for(new h=1;h<iNum;h++)
						{
							new i=iPlayers[h]
#if defined TEST_TP
							if(id==i) // for testing puroses without someone else in server
#else
							if(id!=i)
#endif
							{
#if defined EVENT_DMG
								if(is_user_alive(i) && !PlayerIsHurt[i]) // team power won't run if team mate is hurt
#else
								if(is_user_alive(i))
#endif
								{
									// changed all "id" values to "i" because it was not healing team mates
									distanceRange = 600+speed[id]+dist[id]+medals[id]
									new Float:origin_i[3]
									pev(i,pev_origin,origin_i)
									new Float:origin_id[3]
									pev(id,pev_origin,origin_id)
									if(get_distance_f(origin_i,origin_id)<=distanceRange)
									{
										health_values = health[i] + 100 + (medals[i]-1);
										if(get_user_health(i)<health_values)
										{
											set_user_health(i,get_user_health(i)+1)
										}
#if defined ARMOR_TP
										armor_values = armor[i] + 100 + medals[i];
										if(get_user_armor(i)<armor_values)
										{
											set_user_armor( i, get_user_armor(i)+1);
										}
#endif
										// added ammo team power functionality - speeds up
#if defined AMMO_TP
										// ammo team power boost - if team mate has ammo regen, and player has awareness, it will speed up ammo regen time with team power
											
										// perhaps use team power to increase players health and armor regen as well... only lower levels? 
										// generate armor+hp for lower level players, generate only hp for higher level players, 
										// and only increase armor and health regen for highest levels?
										if (rammo[i]>0&&speed[id]>0)
										{
											ammowait[i]+=medals[i]+medals[id]
										}
#endif
									// bonus chance can go here - swmpdg
									}
								}
							}
						}
					}
				}
#endif
#endif

#if !defined USING_SVEN
				/* CS code begin */
				if(dist[id]>0)
				{
					get_players(iPlayers,iNum)
#if defined TEST_TP
					if(iNum>=1) // for testing puroses without someone else in server
#else
					if(iNum>1)
#endif
					{
						for(new h=0;h<iNum+1;h++)
						//for(new h=0;h<iNum;h++)
						//for(new h=1;h<iNum;h++)
						{
							new i=iPlayers[h]
#if defined TEST_TP
							if(id==i) // for testing puroses without someone else in server
#else
							if(id!=i)
#endif
							{
#if defined EVENT_DMG
								if(is_user_alive(i) && !PlayerIsHurt[i]) // team power won't run if team mate is hurt
#else
								if(is_user_alive(i))
#endif
								{
									new teama[32]
									new teamb[32]
									get_user_team(id,teama,31) // will this work for gangwars?...
									get_user_team(i,teamb,31)
									if(equali(teama,teamb))
									{
										distanceRange = 600+speed[id]+dist[id]+medals[id]
										new Float:origin_i[3]
										pev(i,pev_origin,origin_i)
										new Float:origin_id[3]
										pev(id,pev_origin,origin_id)
										if(get_distance_f(origin_i,origin_id)<=distanceRange)
										{
											health_values = health[i] + 100 + (medals[i]-1);
											if(get_user_health(i)<health_values)
											{
													set_user_health(i,get_user_health(i)+1)
											}
#if defined ARMOR_TP
#if !defined USING_GW
											armor_values = armor[i] + 100 + (medals[i]-1);
											if(get_user_armor(i)<armor_values)
											{
#if defined USING_CS
												cs_set_user_armor( i, get_user_armor(i)+1, CS_ARMOR_VESTHELM );
#endif
											}
#endif
#endif
#if defined AMMO_TP
											// ammo team power boost - if team mate has ammo regen, and player has awareness, it will speed up ammo regen time with team power
											
											// perhaps use team power to increase players health and armor regen as well... only lower levels? 
											// generate armor+hp for lower level players, generate only hp for higher level players, 
											// and only increase armor and health regen for highest levels?
											if (rammo[i]>0&&speed[id]>0)
											{
												ammowait[i]+=medals[i]+medals[id]
											}
#endif
										// bonus chance can go here - swmpdg
										}
									}
								}
							}
						}
					}
				}
#endif

#if defined EVENT_DMG				
			}
#endif
			if(!is_player_flag_set(has_godmode, id))
			{
				if(dodge[id]>0)
				{
					luck=random_num(0,250+dodge[id]+medals[id])
					health_values = 100+speed[id]+medals[id] // include awareness in blockattack rather than health?
					// set block attack for lower level players higher rate of frequency?
					// health_values = 100+health[id]+medals[id]
					if (get_user_health(id) < health_values && luck >= 200)
					{
						set_task(0.1, "godmode_on", id)
					}
				}
			}
#if defined COUNT_CTRL
			// counts towards scxpm_reexp, should be at least every 2 seconds?
			// scxpm_regen runs every 1.0 seconds as of right now, maybe hard-code a frequency? #define REGENFREQ 1.0
			// could use switch here as a counter for level 0 players...
			if (count_reexp[id]>=get_pcvar_num(pcvar_counter)) 
			{
#if defined USING_SVEN
				if(maxlvl_count[id]!=1)
				{
					// only sven coop needs scxpm_reexp
					// maxlvl_count is for pcvar_maxlevel - if set to 1, it will not run scxpm_reexp. Max level player will only run scxpm_reexp once.
					scxpm_reexp(id)
				}
#endif
				// reset counter
				count_reexp[id] = 0
				if(!is_user_bot(id))
				{
					// showdata to client here - updated every 1*get_pcvar_num(pcvar_counter) seconds
					scxpm_showdata(id)		
				}

			}
			else if(count_reexp[id]<get_pcvar_num(pcvar_counter))
			{
				count_reexp[id]+=1
			}
#endif
#if defined PLYR_CTRL
		}
		else if(playerlevel[id]==0 && rank[id]==0)
		{
			if (!onecount[id])
			{
				onecount[id] = true
			}
			else
			{
#if defined USING_SVEN
				scxpm_reexp(id); // reads experience and updates if they gain experience
#endif
				if(!is_user_bot(id))
				{
					scxpm_showdata(id); // shows data on screen in hud for level 0 player
				}
				onecount[id] = false;
			}
		}
		// don't give xp until scxpm is loaded??
		else if(playerlevel[id]==0 && rank[id]==22 && !is_user_bot(id))
		{
			if (!onecount[id])
			{
				onecount[id] = true
			}
			else
			{
				scxpm_showdata(id); // shows data on screen in hud for level 0 player
				onecount[id] = false;
			}
		}
#endif
	}
}
public godmode_on(id)
{
	if (is_user_connected(id) && is_user_alive(id))
	{
		set_user_godmode(id,1)
	
		set_task(2.0, "godmode_off", id)
	}
}
public godmode_off(id)
{
	if (is_user_connected(id) && is_user_alive(id))
	{
		set_user_godmode(id)
	}
}
/* AMXX-Studio Notes - DO NOT MODIFY BELOW HERE
*{\\ rtf1\\ ansi\\ deff0{\\ fonttbl{\\ f0\\ fnil Tahoma;}}\n\\ viewkind4\\ uc1\\ pard\\ lang1031\\ f0\\ fs16 \n\\ par }
*/