// why is this Health message needed - for the HUD?
/*
#if !defined USING_SVEN
public message_Health(msgid, dest, id) {
	if(!is_user_alive(id) || is_user_bot(id) || is_user_hltv(id) ) {
		return PLUGIN_CONTINUE;
	}
	new hp = get_msg_arg_int(1);
	if(hp > 255 && (hp % 256) == 0) {
		set_msg_arg_int(1, ARG_BYTE, ++hp);
	}
	return PLUGIN_CONTINUE;
}
#endif
*/
public on_ResetHUD( id )
{
// may not work unless allow bot event is turned on, remove this check and see.
//#if !defined ALLOW_BOTS
//	if(!is_user_bot(id))
//	{
//#endif
#if defined EVENT_DMG
		PlayerIsHurt[id] = false;
#endif
#if defined SPEED_CTRL
	// this seemed to mess with the damage event code... set back to code below
	/*
		if (get_pcvar_num(pcvar_speedctrl) == 1)
		{
			if(g_punished[id])
			{
				g_hasSpeed[id] = true;
			}
			else
			{
				g_hasSpeed[id] = false;
			}
			UserSpeed(id)
		}
	*/

	// reset speed on spawn
#if defined EVENT_DMG
		if (get_pcvar_num(pcvar_speedctrl) == 1 && !PlayerIsHurt[id] && g_hasSpeed[id] && !g_punished[id])
#else
		if (get_pcvar_num(pcvar_speedctrl) == 1 && g_hasSpeed[id] && !g_punished[id])
#endif
		{
			g_hasSpeed[id] = false
			UserSpeed(id)
		}
#endif

#if !defined USING_CS
#if !defined USING_GW
#if defined COUNT_CTRL
		count_reexp[id]=get_pcvar_num(pcvar_counter)-1;
#endif
#endif
#endif
		if (get_pcvar_num(pcvar_gravity) >= 1 && gravity[id]>0)
		{
			gravity_enable(id);
		}
		else if(get_pcvar_num(pcvar_gravity) == 0 && gravity[id]>0)
		{
			if(!is_user_bot(id))
			{
				client_print(id, print_chat, "[SCXPM] Anti-Gravity Device is disabled. %i skill points are available for you to use on another skill.", gravity[id])
				client_print(id, print_chat, "[SCXPM] Say /removegravity to remove your gravity skill points")
			}		
			//skillpoints[id]+=gravity[id];
			//gravity[id]=0;
		}
		if (get_user_health(id) < 100+health[id] && health[id]>0)
		{
			set_user_health( id, health[id] + 100 );
		}
#if !defined USING_GW
		if (get_user_armor(id) < 100+armor[id] && armor[id]>0)
		{
#if !defined USING_CS
		set_user_armor( id, armor[id] + 100 );
#else
		cs_set_user_armor(id, armor[id]+100, CS_ARMOR_VESTHELM)
#endif
		}
#endif
	//	if ( get_pcvar_num( pcvar_save_frequent ) == 1 ) 
	//	{
	//		if(is_user_alive(id) && !is_user_bot(id) && !is_user_hltv(id))
	//		{
	//			SavePlayerData( id );
	//			if (get_pcvar_num( pcvar_debug ) == 1 )
	//			{
	//				log_amx( "[SCXPM DEBUG] Client XP Saved on Spawn" );
	//			}
	//		}
	//	}
		if(skillpoints[id] > 0)
		{
			if (spawnmenu[id])
			{
				client_print(id,print_chat,"[SCXPM] You have %i skillpoints available to use. Say /spawnmenu to disable or enable skills menu on spawn.",skillpoints[id])
				SCXPMSkill(id);
			}
		}
//#if !defined ALLOW_BOTS
//	}
//#endif
}
#if defined USING_SVEN
public scxpm_reexp(id) {
//#if defined ALLOW_BOTS
	if(is_user_connected(id))
//#else
//	if(is_user_connected(id) && !is_user_bot(id))
//#endif
	{
		if(playerlevel[id]<get_pcvar_num(pcvar_maxlevel))
		{
			// may want to remove firstLvl - is it even needed? possibly use "death()" of CS Code for using calculation to make more simple..
			// this firstlvl may not be needed at all....
			if(firstLvl[id] == 0)
			{
				firstLvl[id] = playerlevel[id];
			}
			if (get_pcvar_num(pcvar_maxlevelup_enabled) == 0 || playerlevel[id] <= get_pcvar_num(pcvar_maxlevelup_limit) || (playerlevel[id] - firstLvl[id]) < get_pcvar_num(pcvar_maxlevelup))
			{
				new Float:helpvar = float(xp[id])/5.0/get_pcvar_float(pcvar_xpgain)+float(get_user_frags(id))-float(lastfrags[id]);
				xp[id]=floatround(helpvar*5.0*get_pcvar_float(pcvar_xpgain))
				/*
				//	new oldXP = xp[id]
				if(get_user_frags(id) > 0)
				{
					xp[id]=floatround(helpvar*5.0*get_pcvar_float(pcvar_xpgain));
				}
				else
				{
					// remove oldxp on frags lol wtf?
					xp[id]=floatround(helpvar*5.0*get_pcvar_float(pcvar_xpgain))-oldXP;
				}
				*/
			}
			if ( get_pcvar_num( pcvar_save_frequent ) == 1 ) 
			{
				SavePlayerData( id );
			}
			lastfrags[id] = get_user_frags(id);
#if defined FRAGBONUS
			if (lastfrags[id]>=(get_pcvar_num(pcvar_fraglimit)) && medal_limit[id]!=0)
			{
				count_frags(id);
			}
#endif
			if( neededxp[id] > 0 )
			{
				if(xp[id] >= neededxp[id])
				{
					new playerlevelOld = playerlevel[id];
					playerlevel[id] = scxpm_calc_lvl(xp[id]);
					skillpoints[id] += playerlevel[id] - playerlevelOld;
					scxpm_calcneedxp(id);
					new name[32];
					get_user_name( id, name, 31 );
					if (playerlevel[id] < get_pcvar_num(pcvar_maxlevel))
					{
						client_print(id,print_chat,"[SCXPM] Congratulations, %s, you are now Level %i - Next Level: %i XP - Needed: %i XP",name,playerlevel[id],neededxp[id],neededxp[id]-xp[id])
						if (get_pcvar_num( pcvar_debug ) == 1 )
						{
							log_amx("[SCXPM] Player %s reached level %i!", name, playerlevel[id] );
						}
						// perhaps insert SavePlayerData here, on death, and on reset hud, instead of constant saving every second...
						// implementing "scxpm_savectrl" cvar to use with regen loop - count_save[id] will count until it reaches pcvar_savectrl, and will save player data
						scxpm_getrank(id);
						SCXPMSkill(id);
#if defined COUNT_CTRL
						// with pcvar_savectrl set to 1, all players lower than specified rank will only recieve +1 to count_save, and save every other time they level.
						// seems fair for players lower than level 50-100(variable by scxpm_xpgain) who don't take a lot of xp, with rank < 3 (less than level 50)
						if (rank[id]<3 && count_save[id]>=get_pcvar_num(pcvar_savectrl))
						{
							SavePlayerData( id );
							count_save[id] = 0;
						}
						if (rank[id]<3 && count_save[id]<get_pcvar_num(pcvar_savectrl))
						{
							count_save[id]+=1
						}
						else if(rank[id]>=3)
						{
							SavePlayerData( id );
							count_save[id] = 0;
						}						
#endif
					}
					else
					{
						client_print(0,print_chat,"[SCXPM] Everyone say ^"Congratulations!!!^" to %s, who has reached Level %i!",name, get_pcvar_num(pcvar_maxlevel))
						log_amx("[SCXPM] Player %s reached level %i!", name, get_pcvar_num(pcvar_maxlevel) );
						scxpm_getrank(id);
						SCXPMSkill(id);
						SavePlayerData( id );
#if defined COUNT_CTRL
						count_save[id] = 0;
#endif
					}
				}
			}
		}
		else if(playerlevel[id]==get_pcvar_num(pcvar_maxlevel) && maxlvl_count[id]==0)
		{
			// i need to run this again for medal/xp trading .. it won't work for max level lol
			// call for maxlvl_count[id] to be set to 0, and call scxpm_reexp?
			xp[id] = scxpm_calc_xp( playerlevel[id] );
			maxlvl_count[id]=1;
			scxpm_showdata(id);
		}
	}
}
#else
public death() {
	// possibility to save bots' data by using scxpm_save or scxpm_savestyle to set to save with names instead of steamid or ip
    new killerId, victimId;
    killerId = read_data(1);
    victimId = read_data(2);
    /*
    if ( get_pcvar_num( pcvar_debug ) == 1 )
    {
        new nickname[35];
        server_print( "[SCXPM DEBUG] death is triggered." );
        server_print( "[SCXPM DEBUG] killerId: %d",killerId );
        get_user_name( killerId, nickname, 34 );
        server_print( "[SCXPM DEBUG] killer name: %s", nickname );
        server_print( "[SCXPM DEBUG] victimId: %d",victimId );
        get_user_name( victimId, nickname, 34 );
        server_print( "[SCXPM DEBUG] victim name: %s", nickname );
    }
    */
#if defined ALLOW_BOTS
    if (!is_user_connected(killerId) || killerId == victimId ) //  it is trying to register all the bots at once...
#else
	if (!is_user_connected(killerId) || killerId == victimId || is_user_bot(killerId)) // plugin_handled for bot
#endif			
	{
		// turning scxpm_debug off will stop this message from showing up when a bot kills a player or another bot
        if ( get_pcvar_num( pcvar_debug ) == 1 )
        {
            log_amx("[SCXPM DEBUG] Suicide or invalid killer/victim, dont award xp for kill");
			// can remove xp for suicide here?
        }
        return PLUGIN_HANDLED;
    }
	
	if ( playerlevel[killerId] < get_pcvar_num( pcvar_maxlevel ) )
	{
        scxpm_kill( killerId );
    }
#if defined EVENT_DMG
	PlayerIsHurt[victimId] = false;
#endif
#if defined SPEED_CTRL
	if (get_pcvar_num(pcvar_speedctrl) == 1 && !g_punished[victimId])
	{
		g_hasSpeed[victimId] = false
	}
#endif
	return PLUGIN_HANDLED;
}  
public scxpm_kill( id ) {
// bot control here may be a bad idea. No need for bot check here since bot check was ran before calling this function, so bots will never pass through scxpm_kill
	xp[id] += floatround( 5.0 * get_pcvar_float( pcvar_xpgain ) );
	if ( get_pcvar_num( pcvar_save_frequent ) == 1 && !is_user_hltv(id) )
	{
		SavePlayerData( id );
	}
	scxpm_calcneedxp(id);
	lastfrags[id] = get_user_frags(id);
#if defined FRAGBONUS
	if (lastfrags[id]>=(get_pcvar_num(pcvar_fraglimit)) && medal_limit[id]!=0)
	{
		count_frags(id);
	}
#endif	
	if( neededxp[id] > 0 && !is_user_hltv(id))
	{
		// add in the negative xp punishment here for people who kill team mates, friendly npcs?
		if( xp[id] >= neededxp[id] )
		{
			new playerlevelOld = playerlevel[id];
			playerlevel[id] = scxpm_calc_lvl( xp[id] );
			skillpoints[id] += playerlevel[id] - playerlevelOld;
			scxpm_calcneedxp( id );
			new name[64];
			get_user_name( id, name, 63 );
			if ( playerlevel[id] == 1800 )
			if(playerlevel[id] < get_pcvar_num(pcvar_maxlevel))
			{
				client_print(id,print_chat,"[SCXPM] Congratulations, %s, you are now Level %i - Next Level: %i XP - Needed: %i XP",name,playerlevel[id],neededxp[id],neededxp[id]-xp[id])
				if (get_pcvar_num( pcvar_debug ) == 1 )
				{
					log_amx("[SCXPM] Player %s reached level %i!", name, playerlevel[id] );
				}

			}
			else
			{
				client_print(0,print_chat,"[SCXPM] Everyone say ^"Congratulations!!!^" to %s, who has reached Level %i!",name, get_pcvar_num(pcvar_maxlevel))
				log_amx("[SCXPM] Player %s reached level %i!", name, get_pcvar_num(pcvar_maxlevel) );
			}
			scxpm_getrank( id );
			SCXPMSkill( id );
			SavePlayerData(id);
		}
	}
}
#endif

#if defined EVENT_DMG
//------------------
//	EVENT_Damage()
//------------------

public EVENT_Damage(id)
{
#if defined ALLOW_BOTS
	if (!PlayerIsHurt[id] && is_user_alive(id) && is_user_connected(id))
#else
	// adding "is_user_alive" and "is_user_connected" check to hopefully stop FUN Runtime Error 10 Invalid Player
	if(!PlayerIsHurt[id] && !is_user_bot(id) && is_user_alive(id) && is_user_connected(id))
#endif
	{
		PlayerIsHurt[id] = true
#if defined SPEED_CTRL
		if (get_pcvar_num(pcvar_speedctrl) == 1)
		{
			if (!g_punished[id])
			{
				g_hasSpeed[id] = true
				UserSpeed(id)
			}
		}
#endif
#if defined GRAV_DMG
		if (gravity[id]>0 && get_pcvar_num(pcvar_gravity) == 1)
		{
			set_user_gravity(id, 1.0)
		}
#endif
#if defined SPEED_CTRL
		if (!g_punished[id])
		{
			set_task(5.0, "reset_status", id)
		}
		else
		{
			set_task(5.0, "reset_regen", id)
		}
#else
		set_task(5.0, "reset_regen", id)
#endif
	}
	return PLUGIN_CONTINUE;
}
public reset_status(id)
{
	PlayerIsHurt[id] = false
#if defined SPEED_CTRL
	if (get_pcvar_num(pcvar_speedctrl) == 1)
	{
		g_hasSpeed[id] = false
		UserSpeed(id)
	}
#endif
#if defined GRAV_DMG
	if (gravity[id]>0 && get_pcvar_num(pcvar_gravity) == 1)
	{
		gravity_enable(id)
	}
#endif
}
public reset_regen(id)
{
	// this function is only for "punished" players
	PlayerIsHurt[id] = false
#if defined GRAV_DMG
	if (gravity[id]>0 && get_pcvar_num(pcvar_gravity) == 1)
	{
		gravity_enable(id)
	}
#endif
}
#endif
#if defined ALLOW_TRADE
public medal_trade(id)
{
	if (trade_limit[id]!=0)
	{
		if (medals[id]>1)
		{
			fragbonus = (get_pcvar_num(pcvar_bonus)*(rank[id]+1))
			// now add the xp to the current xp
			xp[id] += fragbonus;
			medals[id]-=1
			trade_limit[id]-=1
			new oldLevel = playerlevel[id];
			// now save the stats
			// hopefully adding this here will properly calculate level for player
			playerlevel[id] = scxpm_calc_lvl ( xp[id] );
			scxpm_calcneedxp(id);
			if (playerlevel[id]>=get_pcvar_num(pcvar_maxlevel))
			{
				if(maxlvl_count[id]!=0)
				{
					maxlvl_count[id] = 0
				}
			}
			SavePlayerData( id );
			if (playerlevel[id] < oldLevel && skillpoints[id] > 0)
			{
				skillpoints[id]-=1
				
			}
			if (get_pcvar_num( pcvar_debug ) == 1 )
			{
				// for logging purposes
				new tradename[32];
				new tradeid[32];
				get_user_name( id, tradename, 31 );
				get_user_authid(id, tradeid, 31 );
				log_amx("[SCXPM] %s %s traded %i XP and has %i Medals remaining.", tradename, tradeid, fragbonus, medals[id]-1 );
				console_print( id, "%s gained %i xp. New xp: %i", tradename, fragbonus, xp[id] );
			}
			client_print(id, print_chat, "You traded a Medal for %i XP. You have %i Medals remaining.", fragbonus, medals[id]-1)
		}
		else
		{
			client_print(id, print_chat, "You do not have any Medals to trade")
		}
	}
	else
	{
		client_print(id, print_chat, "You have already reached the maximum trade limit for this map")
	}
}
public xp_trade(id)
{
	if (trade_limit[id]!=0)
	{
		fragbonus = (get_pcvar_num(pcvar_bonus)*(rank[id]+1))
		if (medals[id]<16 && xp[id]>=fragbonus)
		{
			// now add the xp to the current xp
			xp[id] -= fragbonus;
			medals[id]+=1;
			trade_limit[id]-=1;
			new oldLevel = playerlevel[id]
			playerlevel[id] = scxpm_calc_lvl ( xp[id] );
			scxpm_calcneedxp(id);
			scxpm_getrank( id );
			SavePlayerData( id );
			if (oldLevel < playerlevel[id])
			{
				SCXPMSkill( id );
			}
			if (playerlevel[id]>=get_pcvar_num(pcvar_maxlevel))
			{
				if(maxlvl_count[id]!=0)
				{
					maxlvl_count[id] = 0
				}
			}
			if (get_pcvar_num( pcvar_debug ) == 1 )
			{
				// for logging purposes
				new tradename[32];
				new tradeid[32];
				get_user_name( id, tradename, 31 );
				get_user_authid(id, tradeid, 31 );
				log_amx("[SCXPM] %s %s traded %i XP for a Medal.", tradename, tradeid, fragbonus);
				console_print( id, "%s traded %i xp. New xp: %i", tradename, fragbonus, xp[id] );
			}
			client_print(id, print_chat, "You traded %i XP for a medal. You now have %i Medals.", fragbonus, medals[id]-1)
		}
		else if(medals[id]==16 && xp[id]>=fragbonus)
		{
			client_print(id, print_chat, "You already have 15 medals.")
		}
		else
		{
			client_print(id, print_chat, "You do not have enough XP to trade for a Medal.")
		}
	}
	else
	{
		client_print(id, print_chat, "You have already reached the maximum trade limit for this map")
	}
}
#endif
#if defined FRAGBONUS
// going to use scxpm_reexp as the function to trigger the medal giving?
// idea to increase pcvar_fraglimit by a multiplier of levels based on player level or rank - costs more frags to get medal, but also gives more xp when buying xp with medal?
public count_frags(id)
{
	if (medals[id]<16&&medals[id]>=1)
	{
		// fragcount = (get_pcvar_num(pcvar_fraglimit)+rank[id])
		fragcount = (get_pcvar_num(pcvar_fraglimit))
		if (lastfrags[id] >= fragcount)
		{
			// new oldfrags = get_user_frags(id);
			// we may get some serious bugs here
			resetfrags = (lastfrags[id]-fragcount);
			//resetfrags = (lastfrags[id]-(get_pcvar_num(pcvar_fraglimit))
			medals[id]+=1
			client_print(id, print_chat, "You won a Medal for getting %i frags. Your frags will be reset to %i.", fragcount, resetfrags)
			medal_limit[id]=0
			set_user_frags(id, resetfrags);
			lastfrags[id] = resetfrags;
			if (get_pcvar_num( pcvar_debug ) == 1 )
			{
				// for logging purposes
				new tradename[32];
				new tradeid[32];
				get_user_name( id, tradename, 31 );
				get_user_authid(id, tradeid, 31 );
				log_amx("[SCXPM] %s %s won a Medal for getting %i frags. Frags will be reset to %i.", tradename, tradeid, fragcount, resetfrags );
			}
		}
		else
		{
			if (get_pcvar_num( pcvar_debug ) == 1 )
			{
				new tradename[32];
				new tradeid[32];
				get_user_name( id, tradename, 31 );
				get_user_authid(id, tradeid, 31 );
				new fragcount=((get_pcvar_num(pcvar_fraglimit)+rank[id])-(get_pcvar_num(pcvar_fraglimit)))
				client_print(id, print_chat, "You need %i more frags to get a Medal.", fragcount-lastfrags[id])
				log_amx("%s %s needs %i more frags to get a Medal.", tradename, tradeid, fragcount-lastfrags[id])
			}
		}
	}
	else if(medals[id]==16)
	{
		// fragcount = (get_pcvar_num(pcvar_fraglimit)+rank[id])
		fragcount = (get_pcvar_num(pcvar_fraglimit))
		if (lastfrags[id] >= fragcount)
		{
			fragbonus = (get_pcvar_num(pcvar_bonus)*(rank[id]+1))
			resetfrags = (lastfrags[id]-fragcount)
			medal_limit[id]=0
			xp[id] += fragbonus;
			set_user_frags(id, resetfrags)
			client_print(id, print_chat, "You already have %i Medals and cannot hold any more", medals[id]-1, fragbonus)
			client_print(id, print_chat, "%i XP will be credited for getting %i frags. Your frags will be reset to %i.", fragbonus, fragcount, resetfrags)
			if (get_pcvar_num( pcvar_debug ) == 1 )
			{
				// for logging purposes
				new tradename[32];
				new tradeid[32];
				get_user_name( id, tradename, 31 );
				get_user_authid(id, tradeid, 31 );
				log_amx("[SCXPM] %s %s already has %i Medals and cannot hold any more.", tradename, tradeid, medals[id]-1);
				log_amx("[SCXPM] %i XP will be credited to %s for getting %i frags.", fragbonus , tradename, fragcount );
				// console_print( id, "%s gained %i xp. New xp: %i", tradename, fragbonus, xp[id] );
			}
		}
	}
	// BUG FIX!! LOL Forgot to do the check about for if player has 15 medals.
	// error check
	else if(medals[id]<=-1)
	{
		medals[id]=0
	}
/*
else
{
	client_print(id, print_chat, "You already won a medal in this map.")
	if (get_pcvar_num( pcvar_debug ) == 1 )
	{
		// for logging purposes
		new tradename[32];
		new tradeid[32];
		get_user_name( id, tradename, 31 );
		get_user_authid(id, tradeid, 31 );
		log_amx("[SCXPM] %s %s already won a Medal on this map.", tradename, tradeid);
		// console_print( id, "%s gained %i xp. New xp: %i", tradename, fragbonus, xp[id] );
	}
}
*/
}
#endif
/* AMXX-Studio Notes - DO NOT MODIFY BELOW HERE
*{\\ rtf1\\ ansi\\ deff0{\\ fonttbl{\\ f0\\ fnil Tahoma;}}\n\\ viewkind4\\ uc1\\ pard\\ lang1031\\ f0\\ fs16 \n\\ par }
*/