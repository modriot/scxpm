// "main.inl" - may need to move some functions to "xp.inl" for xp handling organization

public plugin_end() {
	if (get_pcvar_num( pcvar_debug ) == 1 )
	{
		log_amx( "[SCXPM DEBUG] plugin_end" );
	}
	if ( dbc ) {
		SQL_FreeHandle( dbc );
	}
	plugin_ended = true;
	return PLUGIN_HANDLED;
}
#if !defined USING_SVEN
// set gamename
public scxpm_gn() { 
	if( get_pcvar_num(pcvar_gamename) >= 1 )
	{
		new g[32];
		format( g, 31, "SCXPM %s", VERSION );
		forward_return( FMV_STRING, g);
		return FMRES_SUPERCEDE;
	}
	return PLUGIN_HANDLED;
}
#endif
//#if defined USING_SVEN
// added by swmpdg to slow down connection attempt
public plugin_cfg()
{
	// this seems to be very important here, loaded before sql_init - swmpdg
	for( new i = 0; i<33 ;i++ )
	{
		clear_player_flag(loaddata, i);
	}
	plugin_ended = false;
#if defined SQLCVAR
//#if defined USING_SVEN
	new configsDir[64];
	get_configsdir(configsDir, 63);
	server_cmd("exec %s/xp-sql.cfg", configsDir);
//#endif
#endif
	if (get_pcvar_num(pcvar_debug) == 1)
	{
		log_amx( "[SCXPM] Loading Sven Coop XP Mod version %s", VERSION );
	}
	// incrementally changing task time seconds to load after map loads completely in logs
	set_task( 1.0, "sql_init" ); // timer for sql init - after plugin completely loads instead of in plugin_init
#if defined USING_SVEN
	if (get_pcvar_num( pcvar_debug ) == 1 )
	{
		set_task(10.0, "gravity_check"); // check for gravity to be loaded or unloaded - 5 seconds not long enough, may need 10-15 seconds?
	}
#endif
	// set so only loads data after client connects and is put in server...
	// set to load at least .4 seconds after - allowing sql_init to load?
}
public gravity_check()
{
// doesn't load cvar fast enough... even 5 seconds is not long enough for the set_task
	if (get_pcvar_num( pcvar_gravity ) == 0 )
	{
		log_amx("[SCXPM DEBUG] Anti-Gravity has been disabled on this map" );
	}
	else if (get_pcvar_num( pcvar_gravity ) == 1 )
	{
		log_amx("[SCXPM DEBUG] Anti-Gravity has been enabled on this map")
	}
}
public gravity_msg(id)
{
	if (is_user_connected(id))
	{
		if (get_pcvar_num( pcvar_gravity ) == 0 )
		{
			client_print(id, print_chat, "[SCXPM] An alien force disabed your Anti-Gravity Device on this map");
		}
		else
		{
			gravity_enable(id);
			client_print(id, print_chat, "[SCXPM] Anti-Gravity Device has been enabled on this map");
		}
	}
	if ( get_pcvar_num( pcvar_save ) == 2 )
	{
		set_task(5.0, "scxpm_newbiehelp", id)
	}
}
public load_hpap(id)
{
	if(is_user_connected(id) && is_user_alive(id))
	{
		if (get_user_health(id) < health[id]+100+medals[id]-1)
		{
			set_user_health( id, health[id] + 100 + medals[id]-1)
		}
#if !defined USING_GW
		if (get_user_armor(id) < armor[id]+100+medals[id]-1)
		{
#if defined USING_CS
			cs_set_user_armor(id, armor[id]+100+medals[id]-1, CS_ARMOR_VESTHELM)
#else
			set_user_armor( id, armor[id] + 100 + medals[id]-1)
#endif
		}
#endif
	}
}
public gravity_enable(id)
{
	if(is_user_alive(id) && is_user_connected(id) && gravity[id] > 0 && medals[id]>0)
	{
		set_user_gravity( id, 1.0 - ( 0.015 * gravity[id] ) - ( 0.001 * medals[id]) );
	}
	else if(is_user_alive(id) && is_user_connected(id) && gravity[id] > 0)
	{
		set_user_gravity( id, 1.0 - ( 0.015 * gravity[id] ));
	}
}
/*
public gravity_disable(id)
{
#if !defined ALLOW_BOTS
	if(is_user_alive(id) && is_user_connected(id) && !is_user_hltv(id) && !is_user_bot(id) && gravity[id] > 0)
#else
	if(is_user_alive(id) && is_user_connected(id) && !is_user_hltv(id) && gravity[id] > 0)
#endif
	{
		set_user_gravity(id, 1.0)
	}
}
*/
#if defined SPEED_CTRL
public UserSpeed(id)
{
	// trying to add "is_user_alive" and "is_user_connected" here to prevent FUN Runtime Error 10? ("invalid player")
	if (g_hasSpeed[id] && is_user_alive(id) && is_user_connected(id))
	{
		// may not need float value here???
		new Float:oldSpeedValue = get_user_maxspeed(id)
		new reducedSpeed = get_pcvar_num(pcvar_reduce)
		set_user_maxspeed(id, oldSpeedValue-reducedSpeed)
	}
	//else if (!g_hasSpeed[id] && !g_punished[id])
	else if (!g_hasSpeed[id] && is_user_alive(id) && is_user_connected(id))
	{
		// may not need to use Float here. Could just use get_user_maxspeed ???? 
		new Float:oldSpeedValue = get_user_maxspeed(id)
		new reducedSpeed = get_pcvar_num(pcvar_reduce)
		set_user_maxspeed(id, oldSpeedValue+reducedSpeed)
	}
	return PLUGIN_CONTINUE
}
#endif
/* AMXX-Studio Notes - DO NOT MODIFY BELOW HERE
*{\\ rtf1\\ ansi\\ deff0{\\ fonttbl{\\ f0\\ fnil Tahoma;}}\n\\ viewkind4\\ uc1\\ pard\\ lang1031\\ f0\\ fs16 \n\\ par }
*/